#include <OpenSim/OpenSim.h>
#include <OpenSim/Simulation/InverseDynamicsSolver.h>
#include <OpenSim/Common/FunctionSet.h>
#include <OpenSim/Common/Function.h>
#include <OpenSim/Common/Sine.h>
#include <OpenSim/Common/DisplayGeometry.h>

#include "saccadesimulator.h"
#include "saccadesimulatorimpl.h"
#include "osimutils.h"
#include "eyepassiveforce.h"
#include "musclegeomrecorder.h"

using namespace std;
using namespace SimTK;
using namespace OpenSim;

#define IDEAL_ACTUATOR
typedef PathActuator MuscleType;

SaccadeSimulator::SaccadeSimulatorImpl::
    SaccadeSimulatorImpl(const Eye &eyeParams, string resultDir) : 
    resDir(resultDir)
{
    saccadeController = new ForwardSaccadeController();
    createOsimModel(eyeModelRight, eyeParams, "RightEye", false);
    createOsimModel(eyeModelLeft, eyeParams, "LeftEye", true);
    eyeModelRight.print(resultDir + "EyeModel_Right.osim");
    eyeModelLeft.print(resultDir + "EyeModel_Left.osim");
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    createOsimModel (Model &model, const Eye &eye, const string name, bool left)
{
    // Create OpenSim eye model
    model.setUseVisualizer(false);
    model.setName(name);
    addEyeToOpenSimModel(eye, model, left);

    // Add passive eye force
    OpenSim::Body *eyeBody = &model.updBodySet().get("Eye");
    EyePassiveForce *pf = new EyePassiveForce();
    pf->setStiffness(eye.passiveStiffness);
    pf->setViscosity(eye.passiveViscosity);
    model.addForce(pf);
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    createEyeMuscles (const Eye &eye, vector<EyeMuscle> &muscles, bool left)
{
    unsigned i;
    const Vec3 AnnulusOfZinn ( -0.5 * eye.R,             0,  3.0 * eye.R);
    const Vec3 Maxillary     ( -1.2 * eye.R,  -0.8 * eye.R,  0.5 * eye.R);
    const Vec3 Trochlea      ( -1.2 * eye.R,   0.8 * eye.R,  0.5 * eye.R);

    muscles.resize(6);

    i = 0;
    muscles[i++].name = "Medial_Rectus";
    muscles[i++].name = "Lateral_Rectus";
    muscles[i++].name = "Inferior_Rectus";
    muscles[i++].name = "Superior_Rectus";
    muscles[i++].name = "Superior_Oblique";
    muscles[i++].name = "Inferior_Oblique";
    i = 0;
    muscles[i++].eyePoint = Vec3( -eye.R * 0.9,             0,  -eye.R * 0.45);
    muscles[i++].eyePoint = Vec3(  eye.R * 0.9,             0,  -eye.R * 0.45);
    muscles[i++].eyePoint = Vec3(            0,  -eye.R * 0.9,  -eye.R * 0.45);
    muscles[i++].eyePoint = Vec3(            0,   eye.R * 0.9,  -eye.R * 0.45);
    muscles[i++].eyePoint = Vec3( eye.R * 0.45,   eye.R * 0.9,            0.0);
    muscles[i++].eyePoint = Vec3( eye.R * 0.45,  -eye.R * 0.9,            0.0);

    // Same on all extraocular muscles
    for (i=0; i<muscles.size(); i++) {
        muscles[i].gndPoint = AnnulusOfZinn;
        muscles[i].params = eye;
    }

    // Difference in ground point
    muscles[4].gndPoint = Trochlea;
    muscles[5].gndPoint = Maxillary;

    // Mirror X coords for left eye
    if (left) {
        for (unsigned i=0; i<muscles.size(); i++) {
            EyeMuscle &mp = muscles[i];
            mp.eyePoint.set(0, mp.eyePoint[0]*-1);
            mp.gndPoint.set(0, mp.gndPoint[0]*-1);
        }
    }

}

void SaccadeSimulator::SaccadeSimulatorImpl::
    addEyeToOpenSimModel (const Eye &eye, Model &model, bool left)
{
    vector<EyeMuscle> muscleParams;
    createEyeMuscles(eye, muscleParams, left);

    Inertia inertia = eye.mass * Inertia::sphere(eye.R);
    OpenSim::Body *eyeBody = new OpenSim::Body("Eye", eye.mass, Vec3(0), inertia);
    eyeBody->updDisplayer()->setShowAxes(false);
    BallJoint *eyeJoint = new BallJoint("eyeBallJoint", model.getGroundBody(),
    Vec3(0), Vec3(0), *eyeBody, Vec3(0), Vec3(0));
    CoordinateSet& jcs = eyeJoint->upd_CoordinateSet();
    double range_arr[2] = { -eye.range, eye.range };
    jcs[0].setName("eyeBallRotX");
    jcs[1].setName("eyeBallRotY");
    jcs[2].setName("eyeBallRotZ");
    jcs[0].setRange(range_arr);
    jcs[1].setRange(range_arr);
    jcs[2].setRange(range_arr);

    model.addBody(eyeBody);

    // Create wrapping sphere
    WrapSphere *ws = new WrapSphere();
    ws->setName("EyeGlobeWrapSphere");
    (static_cast<PropertyDbl&>(ws->updPropertyByName("radius"))).setValue(eye.R*0.95);
    eyeBody->addWrapObject(ws);

    for (unsigned i = 0; i < muscleParams.size(); i++) {
        const EyeMuscle &muscle = muscleParams[i];
        MuscleType *m = new MuscleType();
        model.addForce(m);
        m->setName(muscle.name);
        char str[32];
        sprintf(str, "_ground_point_%d", 0);
        m->addNewPathPoint(muscle.name + str, model.getGroundBody(), muscle.gndPoint);
        m->addNewPathPoint(muscle.name + "_eye_point", *eyeBody, muscle.eyePoint);
        m->updGeometryPath().addPathWrap(*ws);
        m->setMinControl(0.01);
        m->setMaxControl(1);

        // Don't forget to convert grams to Newtons !!!
#ifdef IDEAL_ACTUATOR
         m->setOptimalForce(muscle.params.muscleParams.forces[i] / 100.0);
#else
        m->setMaxIsometricForce(muscle.params.muscleParams.forces[i] / 100.0);
        m->setMaxIsometricForce(muscle.params.muscleParams.forces[i] / 100.0);
        m->setOptimalFiberLength(muscle.params.optimalFiberLength);
        m->setTendonSlackLength(muscle.params.tendonSlackLength);
        m->setActivationTimeConstant   (0.001);
        m->setDeactivationTimeConstant (0.001);
#endif

    }

}

void SaccadeSimulator::SaccadeSimulatorImpl::
    simulate (Saccade &saccade, bool store, SaccadeController *controller)
{
    SaccadeController *tmpCon = saccadeController;

    if (controller!= NULL)
        saccadeController = controller;

    simulate(eyeModelLeft, saccade.rotFromL, saccade.rotToL, saccade.timesL,
        saccade.rotLx, saccade.rotLy, saccade.rotLz, saccade.velL, saccade.pathsL,
        saccade.activationsL, saccade.timesActL, false);

    simulate(eyeModelRight, saccade.rotFromR, saccade.rotToR, saccade.timesR,
        saccade.rotRx, saccade.rotRy, saccade.rotRz, saccade.velR, saccade.pathsR,
        saccade.activationsR, saccade.timesActR, store);

    saccadeController = tmpCon;
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    simulate (Model &model, const double rotFrom[3], const double rotTo[3],
    vector<double> &times, vector<double> &rX, vector<double> &rY, vector<double> &rZ, 
    vector<double> &vel, vector<EyeMusclePaths> musclePaths,
    vector<EyeMuscleActivations> &acts, vector<double> &actsTimes, bool store)
{
    // Make a local copy of the model.
    OpenSim::Model lmodel(model);

    // Compute activations to drive the saccade.
    const Vec2 rot_i (rotFrom);
    const Vec2 rot_f (rotTo);

    double duration = 0.5;

    vector<OpenSim::Function*> controlFuncs;

    // If the default controller is to be used, compute appropriate controll funcs.
    if (typeid(*saccadeController)==typeid(ForwardSaccadeController)) {
        computeActivations(lmodel, rot_i, rot_f, controlFuncs, duration, store);
        ((ForwardSaccadeController*) saccadeController)->setControlFunctions(controlFuncs);
    }

    // Create OpenSim controller object.
    SaccadeController_ *controller_ = new SaccadeController_(saccadeController);
    controller_->setActuators(lmodel.getActuators());
    lmodel.addController(controller_);

    // Set initial parameters.
    State &state = lmodel.initSystem();
    const BallJoint &ebj =
    static_cast<const BallJoint&>(lmodel.getJointSet().get("eyeBallJoint"));
    const CoordinateSet &jcs = ebj.get_CoordinateSet();
    jcs.get("eyeBallRotX").setValue(state, convertDegreesToRadians(rot_i[0]));
    jcs.get("eyeBallRotY").setValue(state, convertDegreesToRadians(rot_i[1]));

    lmodel.equilibrateMuscles(state);

    // Add reporters
    ForceReporter* forceReporter = new ForceReporter(&lmodel);
    MuscleGeomRecorder *muscleGeomRecorder = new MuscleGeomRecorder(&lmodel);
    lmodel.addAnalysis(forceReporter);
    lmodel.addAnalysis(muscleGeomRecorder);

    // Simulate
    RungeKuttaMersonIntegrator integrator(lmodel.getMultibodySystem());
    Manager manager(lmodel, integrator);
    manager.setInitialTime(0);
    manager.setFinalTime(duration);
    manager.integrate(state);

    // Convert rads to degs
    Storage &saccadeStates = manager.getStateStorage();
    lmodel.getSimbodyEngine().convertRadiansToDegrees(saccadeStates);

    // Write results to out data structures
    rX.clear();
    rY.clear();
    rZ.clear();
    vel.clear();
    times.resize(saccadeStates.getSize());

    for (int i = 0; i<saccadeStates.getSize(); i++) {
        double x,   y,  z;
        double vx, vy, vz;
        saccadeStates.getTime(i, times[i]);

        saccadeStates.getData(i, 0, x);
        saccadeStates.getData(i, 1, y);
        saccadeStates.getData(i, 2, z);

        saccadeStates.getData(i, 3, vx);
        saccadeStates.getData(i, 4, vy);
        saccadeStates.getData(i, 5, vz);

        rX.push_back(x);
        rY.push_back(y);
        rZ.push_back(z);
        vel.push_back(::sqrt(vx*vx + vy*vy));
    }

    // Get activation trajectory
    acts.resize(controller_->getControlLog().size());
    actsTimes.resize(controller_->getControlLog().size());

    vector<int> index(acts.size(), 0);
    for (int i = 0 ; i != index.size() ; i++) {
        index[i] = i;
    }

    sort(index.begin(), index.end(),
        [&](const int& a, const int& b) {
            return (controller_->getControlTimesLog()[a] < 
                    controller_->getControlTimesLog()[b]);
        }
    );

    for (int i = 0 ; i != acts.size() ; i++) {
        acts[i] = controller_->getControlLog()[index[i]];
        actsTimes[i] = controller_->getControlTimesLog()[index[i]];
    }

    // Delete duplicates
    double ii = 0;
    for (int i=1; i<acts.size(); i++) {
        if (actsTimes[i] <= actsTimes[ii]) {
            actsTimes.erase(actsTimes.begin()+i);
            acts.erase(acts.begin()+i);
            --i;
        }
        ii = i;
    }

    // Store simulation products if requested
    if (store) 
    {
        // Make file prefix
        string prfx = resDir + lmodel.getName() + "_" + rot_i.toString().substr(1, -1) + 
            "-" + rot_f.toString().substr(1, -1);
    
        // Write files
        forceReporter->getForceStorage().print(prfx + "_Forces.mot");
        
        muscleGeomRecorder->print(prfx + "_TEST_.txt");

        saccadeStates.print(prfx + "_States.sto");
        
        OsimUtils::writeFunctionsToFile(controlFuncs, 
            prfx + "_Excitations_FUNC.sto", duration, 0.001);

        OsimUtils::writeFunctionsToFile(actsTimes, acts, 
            prfx + "_Excitations_LOG.sto");
    }

}

void SaccadeSimulator::SaccadeSimulatorImpl::
    inverseSimulate(Saccade &saccade, string name)
{
    // Create the state sequence corresponding to the recorded saccade.
    OpenSim::Model lmodel(eyeModelRight);
    State state = lmodel.initSystem();
    Storage states;
    states.setDescription("Recorded Saccade");
    Array<std::string> &stateNames = lmodel.getStateVariableNames();    
    stateNames.insert(0, "time");
    states.setColumnLabels(stateNames);
    Array<double> stateVals;

    for(int i=0; i<saccade.rotRx.size(); i++) {
        state.updQ()[0] = convertDegreesToRadians(saccade.rotRx[i]);
        state.updQ()[1] = convertDegreesToRadians(saccade.rotRy[i]);
        state.updQ()[2] = convertDegreesToRadians(saccade.rotRz[i]);

        lmodel.getStateValues(state,stateVals);
        states.append(saccade.timesR[i], stateVals.size(), stateVals.get());
    }
    states.setInDegrees(false);

    // Perform the static optimization
    StaticOptimization so(&lmodel);
    so.setStatesStore(states);
    int ns = states.getSize();
    double ti, tf;
    states.getTime(0, ti);
    states.getTime(ns-1, tf);
    so.setStartTime(ti);
    so.setEndTime(tf);

    // Static optimization loop
    State s = lmodel.initSystem();
    for (int i = 0; i < ns; i++) {
        states.getData(i, s.getNY(), &s.updY()[0]);
        Real t = 0.0;
        states.getTime(i, t);
        s.setTime(t);
        lmodel.assemble(s);
        lmodel.getMultibodySystem().realize(s, SimTK::Stage::Velocity);

        if (i == 0 ) 
            so.begin(s);
        else if (i == ns)  
            so.end(s);
        else 
            so.step(s, i);
    }

    // Extract activations from storage object
    Storage *as = so.getActivationStorage();
    int na = lmodel.getActuators().getSize();
    int row = as->getSize() - 1;
    saccade.activationsR.resize(states.getSize());

    for (int row = 0; row<states.getSize(); row++)
        for (int i = 0; i<na; i++)
            as->getData(row, i, saccade.activationsR[row].act[i]);

    // Perform validating fwd simulation
    double rotFrom[3] = {saccade.rotRx[0], saccade.rotRy[0], saccade.rotRz[0]};
    double rotTo[3] = {saccade.rotRx.back(), saccade.rotRy.back(), saccade.rotRz.back()};
    vector<double> times, timesAct, rotx, roty, rotz, vel;
    vector<EyeMuscleActivations> acts;
    vector<EyeMusclePaths> paths;
    ValidationSaccadeController controller;
    controller.setControlActivations(saccade.timesR, saccade.activationsR);
    SaccadeController *tmpCon = saccadeController;
    saccadeController = &controller;
    simulate(eyeModelRight, rotFrom, rotTo, times, rotx, roty, rotz, vel, paths, 
        acts, timesAct, 1);
    saccadeController = tmpCon;

    // Store results
    OsimUtils::writeFunctionsToFile(
        saccade.timesR, 
        saccade.activationsR, 
        resDir + name + ".sto"
    );

    lmodel.getSimbodyEngine().convertRadiansToDegrees(states);
    states.setInDegrees(true);
    states.print(resDir + "recorded_states.sto");
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    computeActivations (Model &model, const Vec2 &rot_i, const Vec2 &rot_f,
    vector<OpenSim::Function*> &controlFuncs, double &duration, bool store)
{
    Vector ssai, ssaf;

    calcSSAct(model, rot_i[0], rot_i[1], ssai);
    calcSSAct(model, rot_f[0], rot_f[1], ssaf);

    const int N = 9;

    double angle = sqrt(pow(rot_i[0]-rot_f[0], 2) + pow(rot_i[1]-rot_f[1], 2));

    const double t_eq = 0.000;
    const double dur_xc = (25.0 + 0.95 * angle) / 1000.;
    const double t_fix  = (55.0 + 0.5 * angle) / 1000.;

    double phases[N] = {
       -t_eq,
        0,
        0,
        dur_xc,
        dur_xc * 1.05,
        t_fix,
        t_fix + 0.005,
        t_fix + 0.010,
        t_fix*2
    };
    for (int i=0; i<N; i++) phases[i]+=t_eq;

    // Construct control functions
    controlFuncs.clear();
    double values[N];

    for (int i = 0; i < ssaf.size(); i++)  {
        double dssa = ssaf[i] - ssai[i];
        int j=0;

        values[j++] = ssai[i];                  // Leading delay
        values[j++] = values[j-1];

        values[j++] = ssaf[i] + dssa * 1.15;    // Excess force
        values[j++] = values[j-1];

        values[j++] = ssai[i] + dssa * 1.000;   // Error phase
        values[j++] = values[j-1];

        values[j++] = ssai[i] + dssa * 1.000;   // Fixing phase

        values[j++] = ssaf[i];                  // Steady state phase
        values[j++] = values[j-1];

        OpenSim::Function *controlFunc = new PiecewiseLinearFunction(N, phases, values);
        controlFunc->setName(model.getActuators()[i].getName());
        controlFunc->setName("Excitation_" + model.getActuators()[i].getName());
        controlFuncs.push_back(controlFunc);
    }

    duration = t_eq + t_fix * 2;
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    calcSSAct (Model &model, double rotX, double rotY, Vector &activations)
{
    Real rotx = convertDegreesToRadians(rotX);
    Real roty = convertDegreesToRadians(rotY);
    Real rotz = 0;
    State &state = model.initSystem();
    state.updQ()[0] = rotx;
    state.updQ()[1] = roty;
    state.updQ()[2] = rotz;

    // Perform a dummy forward simulation without forces,
    // just to obtain a state-series to be used by stat opt
    OsimUtils::disableAllForces(state, model);
    RungeKuttaMersonIntegrator integrator(model.getMultibodySystem());
    Manager manager(model, integrator);
    manager.setInitialTime(0);
    manager.setFinalTime(2);
    manager.integrate(state);

    // Perform a quick static optimization that will give us
    // the steady state activations needed to overcome the passive forces
    OsimUtils::enableAllForces(state, model);

    Storage &states = manager.getStateStorage();
    states.setInDegrees(false);
    StaticOptimization so(&model);

    so.setStatesStore(states);
    State &s = model.initSystem();
    states.getData(0, s.getNY(), &s.updY()[0]);
    s.setTime(0);
    so.begin(s);
    so.end(s);

    Storage *as = so.getActivationStorage();
    int na = model.getActuators().getSize();
    activations.resize(na);
    int row = as->getSize() - 1;

    // Store activations to out vector
    for (int i = 0; i<na; i++)
        as->getData(row, i, activations[i]);
}

void SaccadeSimulator::SaccadeSimulatorImpl::
    invDyn (Model &model, FunctionSet &qset,
    Array_<Real> &times, Array_<Vector> &forceTraj, double dur, double step)
{
    // Prepare time series
    InverseDynamicsSolver ids(model);
    times.resize((int)(dur / step) + 1, 0);
    for (unsigned i = 0; i < times.size(); i++)
        times[i] = step * i;

    // Solve for generalized joints forces
    State &s = model.initSystem();
    ids.solve(s, qset, times, forceTraj);
}
