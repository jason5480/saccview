#ifndef EYEPASSIVEFORCE_H
#define EYEPASSIVEFORCE_H

#include <string>
#include <OpenSim/OpenSim.h>
#include <OpenSim/Common/Function.h>
#include <OpenSim/Common/FunctionSet.h>

#include "osimutils.h"

using namespace std;
using namespace SimTK;
using namespace OpenSim;

class EyePassiveForce : public OpenSim::Force {
OpenSim_DECLARE_CONCRETE_OBJECT(EyePassiveForce, Force)

public:
    EyePassiveForce();

    void setStiffness(double stiffness) {_stiffness = stiffness;}

    void setViscosity(double viscosity) {_viscosity = viscosity;}

    void computeForce(const SimTK::State& state,
        SimTK::Vector_<SimTK::SpatialVec>& bodyForces,
        SimTK::Vector& generalizedForces) const;

private:
    double _stiffness, _viscosity;
};

#endif
