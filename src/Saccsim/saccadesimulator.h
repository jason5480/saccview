#ifndef SACCADESIMULATOR_H
#define SACCADESIMULATOR_H

#include "saccadesimulatordll.h"

#include <string>
#include <vector>

using namespace std;

namespace notremor {

struct Point3D 
{
    union
    {
        struct { double x,y,z; };
        double data[3];
    };
    Point3D():x(0), y(0), z(0){}
    Point3D(double x, double y, double z):x(x), y(y), z(z){}
};

struct Eye
{
    // Anatomy
    double R;
    double mass;
    double range;
    double eyesDist;

    // Muscle forces
    union {
        double forces[6];
        struct {
            double force_med_rect;
            double force_lat_rect;
            double force_inf_rect;
            double force_sup_rect;
            double force_sup_obl;
            double force_inf_obl;
        };
    } muscleParams;

    // Muscle params
    double optimalFiberLength;
    double tendonSlackLength;

    // Passive dynamics
    double passiveViscosity;
    double passiveStiffness;
};

struct EyeMuscleActivations
{
    union
    {
        double act[6];
        struct
        {
            double act_medialRectus;
            double act_lateralRectus;
            double act_inferiorRectus;
            double act_superiorRectus;
            double act_superiorOblique;
            double act_inferiorOblique;
        };
    };
};

struct EyeMusclePaths 
{
    vector<Point3D> paths[6];
};

struct Saccade
{
    // IN
    double rotFromR[3], rotToR[3];
    double rotFromL[3], rotToL[3];

    // OUT
    vector<double> timesR, timesL;
    vector<double> rotRx, rotRy, rotRz;
    vector<double> rotLx, rotLy, rotLz;
    vector<double> velR;
    vector<double> velL;
    vector<EyeMusclePaths> pathsR, pathsL;
  
    //Activations
    vector<EyeMuscleActivations> activationsL, activationsR;
    vector<double> timesActR, timesActL;
};

struct SaccadeController
{
    /**
     * Called before every simulation step.
     * You get the current state of the simulation.
     * You can edit the activation.
     * @param time        [IN]  Elapsed time of simulation.
     * @param angles      [IN]  Vector with eyeball angles. (Euler X-Y-Z)
     * @param speeds      [IN]  Vector with eyeball angles' rate of change.
     * @param activations [OUT] Updatable vector of the 6 activations.
     */
    virtual void control (const double time,
        const double rotX,  const double rotY,  const double rotZ,
        const double rotXu, const double rotYu, const double rotZu,
        EyeMuscleActivations &activations) = 0;
};

class saccsim_API SaccadeSimulator
{
public:
    SaccadeSimulator(const Eye &eyeParams, string resDir);

    ~SaccadeSimulator();

    void simulate(Saccade &saccade, bool store, SaccadeController *controller=NULL);

    void inverseSimulate(Saccade &saccade, string name);

    SaccadeController* getController();

private:
    struct SaccadeSimulatorImpl;
    SaccadeSimulatorImpl *impl;
};

}

#endif // SACCADESIMULATOR_H
