#include "eyepassiveforce.h"
#include <string>

using namespace std;

EyePassiveForce::EyePassiveForce()
{
    setName("PassiveEyeForce");
}

void EyePassiveForce::computeForce(const SimTK::State& state,
    SimTK::Vector_<SimTK::SpatialVec>& bodyForces,
    SimTK::Vector& generalizedForces) const
{
    Vec3 rotv(state.getQ()[0], state.getQ()[1], state.getQ()[2]);
    Vec3 spdv(state.getU()[0], state.getU()[1], state.getU()[2]);
    Vec3 torque = -_stiffness * rotv - _viscosity * spdv;
    applyTorque(state, getModel().getBodySet().get("Eye"), torque, bodyForces);
}
