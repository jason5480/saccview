#include "saccadesimulator.h"
#include "saccadesimulatorimpl.h"

using namespace std;

SaccadeSimulator::SaccadeSimulator (const Eye &eyeParams, string resultDir)
{
    impl = new SaccadeSimulatorImpl(eyeParams, resultDir);
}

SaccadeSimulator::~SaccadeSimulator()
{
    delete impl;
}

void SaccadeSimulator::simulate (Saccade &saccade, bool store, 
    SaccadeController *controller)
{
    impl->simulate(saccade, store, controller);
}

void SaccadeSimulator::inverseSimulate (Saccade &saccade, string name)
{
    impl->inverseSimulate(saccade, name);
}

SaccadeController* SaccadeSimulator::getController()
{
    return impl->getController();
}
