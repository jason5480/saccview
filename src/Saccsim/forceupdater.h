#ifndef FORCEUPDATER_H
#define FORCEUPDATER_H

#include <string>
#include <OpenSim/OpenSim.h>
#include <OpenSim/Common/Function.h>
#include <OpenSim/Common/FunctionSet.h>

using namespace std;
using namespace SimTK;
using namespace OpenSim;

class ForceUpdater : public ScheduledEventHandler
{
    const MultibodySystem &_system;
    const Array_<Real> &_times;
    const Array_<Vector> &_forceTraj;
    const SimTK::Force::DiscreteForces *_df;

public:
    ForceUpdater(const MultibodySystem &system, const Array_<Real> &times,
        const Array_<Vector> &forceTraj, const SimTK::Force::DiscreteForces *df) :
        _system(system), _times(times), _forceTraj(forceTraj), _df(df) {
    }

    int findTimeIndex(const Real t) const {
        unsigned i = 0;
        while (i < _times.size() && _times[i] < t) i++;
        return i < _times.size() ? i : -1;
    }

    virtual Real getNextEventTime(const State &s, bool includeCurrentTime) const {
        Real t = s.getTime();
        unsigned i = findTimeIndex(s.getTime());
        if (i >= 0 && i <= _times.size() - (includeCurrentTime ? 1 : 2))
            return _times[i + (includeCurrentTime ? 0 : 1)];
        return -1;
    }

    virtual void handleEvent(State &s, Real accuracy, bool &shouldTerminate) const {
        unsigned i = findTimeIndex(s.getTime());

        if (i < 0 || i >= _times.size() - 1)
            _df->clearAllMobilityForces(s);
        else
            _df->setAllMobilityForces(s, _forceTraj[i]);
    }
};

#endif
