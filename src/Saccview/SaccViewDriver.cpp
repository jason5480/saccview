﻿// QT includes
#include <QApplication>
#include "SaccView.h"

extern int qInitResources_icons();

int main(int argc, char *argv[])
{
	// QT Stuff
	QApplication app(argc, argv);
	
	//qInitResources_icons();
	
	SaccView saccview;
	saccview.show();
	
	return app.exec();
}
