#include <AnimateActors.h>
#include <qdebug.h>

ActorAnimator::ActorAnimator()
{
  this->actor=0;
  this->observer = AnimationCueObserver::New();
  this->observer->animator = this;
}

ActorAnimator::~ActorAnimator()
{
  if(this->actor)
  {
    this->actor->UnRegister(0);
    this->actor=0;
  }
  this->observer->UnRegister(0);
}

void ActorAnimator::SetActor(vtkActor *actor)
{
  if (this->actor)
  {
    this->actor->UnRegister(0);
  }
  this->actor = actor;
  this->actor->Register(0);
}

void ActorAnimator::SetStartPosition(double* pos)
{
  this->startPosition = pos;
}

void ActorAnimator::SetEndPosition(double* pos)
{
  this->endPosition = pos;
}

void ActorAnimator::SetSacccade(Saccade *sac)
{
  saccade = sac;
  numberOfFrames = saccade->timesR.size();
}

void ActorAnimator::SetSpeedFactor(double speed)
{
  speedFactor = speed;
}

void ActorAnimator::AddObserversToCue(vtkAnimationCue *cue)
{
  cue->AddObserver(vtkCommand::StartAnimationCueEvent,this->observer);
  cue->AddObserver(vtkCommand::EndAnimationCueEvent,this->observer);
  cue->AddObserver(vtkCommand::AnimationCueTickEvent,this->observer);
}

void ActorAnimator::Start(vtkAnimationCue::AnimationCueInfo *vtkNotUsed(info))
{
  //this->Actor->SetPosition(this->StartPosition[0],
  //                         this->StartPosition[1], 
  //                         this->StartPosition[2]);
  this->actor->SetOrientation(saccade->rotRx[0],
                              saccade->rotRy[0],
                              saccade->rotRz[0]);
  this->frameCounter = 1;
  this->prevFrame = 0;
  //qDebug() << "Start";
  //qDebug() << "Position: [" << startPosition[0] << ", " << startPosition[1]
  //         << ", " << startPosition[2] << "]";
           
  //qDebug() << "Orientation: [" << saccade->rotRx[0] << ", " << saccade->rotRx[1]
  //         << ", " << saccade->rotRx[2] << "]";
}

void ActorAnimator::Tick(vtkAnimationCue::AnimationCueInfo *info)
{
  //qDebug() << "--TICK EVENT Current Start Frame Number:" << frameCounter << "/" 
  //         << numberOfFrames;
  double elapseTime = (info->AnimationTime - info->StartTime)*speedFactor;
  while (frameCounter<numberOfFrames && elapseTime>=saccade->timesR[frameCounter]) 
    frameCounter++;
  frameCounter--;
  //qDebug() << "Number of Frame selected:" << frameCounter << "Elapse time:" 
  //         << elapseTime << "Frame Time" << saccade->timesR[frameCounter];
  if (frameCounter != prevFrame)
  {
    this->actor->SetOrientation(saccade->rotRx[frameCounter],
                                saccade->rotRy[frameCounter],
                                saccade->rotRz[frameCounter]);
    prevFrame = frameCounter;
    //qDebug() << "Apply";
  }
  //else qDebug() << "Not Apply";
  //*/
}

void ActorAnimator::End(vtkAnimationCue::AnimationCueInfo *vtkNotUsed(info))
{
  //this->Actor->SetPosition(this->EndPosition[0],
  //                         this->EndPosition[1], 
  //                         this->EndPosition[2]);
  //this->actor->SetOrientation(saccade->rotRx[saccade->timesR.size()-1],
  //                            saccade->rotRy[saccade->timesR.size()-1],
  //                            saccade->rotRz[saccade->timesR.size()-1]);

  //qDebug() << "End";
  //qDebug() << "Position: [" << endPosition[0] << ", " << endPosition[1]
  //         << ", " << endPosition[2] << "]";
           
  //qDebug() << "Orientation: [" << saccade->rotRx[saccade->timesR.size()-1] << ", " << saccade->rotRx[saccade->timesR.size()-1]
  //         << ", " << saccade->rotRx[saccade->timesR.size()-1] << "]";
}

ActorAnimator::AnimationCueObserver *ActorAnimator::AnimationCueObserver::New()
{
   return new AnimationCueObserver;
}

void ActorAnimator::AnimationCueObserver::Execute(vtkObject *vtkNotUsed(caller),
                                                  unsigned long event,
                                                  void *calldata)
{
  if(this->animator != 0)
  {
    vtkAnimationCue::AnimationCueInfo *info=
      static_cast<vtkAnimationCue::AnimationCueInfo *>(calldata);
    switch(event)
    {
      case vtkCommand::StartAnimationCueEvent:
        this->animator->Start(info);
        break;
      case vtkCommand::EndAnimationCueEvent:
        this->animator->End(info);
        break;
      case vtkCommand::AnimationCueTickEvent:
        this->animator->Tick(info);
        break;
    }
  }
}

ActorAnimator::AnimationCueObserver::AnimationCueObserver()
{
  this->animator = 0;
}

AnimationSceneObserver *AnimationSceneObserver::New()
{
    return new AnimationSceneObserver;
}

 void AnimationSceneObserver::SetRenderWindow( vtkRenderWindow *renWin)
{
  if (this->RenderWindow)
  {
    this->RenderWindow->UnRegister(this);
  }
  this->RenderWindow = renWin;
  this->RenderWindow->Register(this);

}

void AnimationSceneObserver::Execute(vtkObject *vtkNotUsed(caller),
                                     unsigned long event,
                                     void *vtkNotUsed(calldata))
{
  if(this->RenderWindow != 0)
  {
    switch(event)
    {
      case vtkCommand::AnimationCueTickEvent:
        this->RenderWindow->Render();
      break;
    }
  }
}

AnimationSceneObserver::AnimationSceneObserver()
{
  this->RenderWindow = 0;
}

AnimationSceneObserver::~AnimationSceneObserver()
{
  if(this->RenderWindow)
  {
    this->RenderWindow->UnRegister(this);
    this->RenderWindow = 0;
  }
}