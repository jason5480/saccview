#ifndef _AnimateActors_H_
#include <vtkActor.h>
#include <vtkAnimationCue.h>
#include <vtkCommand.h>
#include <vtkRenderWindow.h>
#include <vector>

#include <saccadesimulator.h>

using namespace notremor;

class ActorAnimator
{
public:
  // Constructor/Destructor
  ActorAnimator();  
  ~ActorAnimator();
  // Methods
  void SetActor(vtkActor *actor);
  void SetStartPosition(double* pos);
  void SetEndPosition(double* pos);
  void SetSacccade(Saccade *sac);
  void SetSpeedFactor(double speed);
  void AddObserversToCue(vtkAnimationCue *cue);
  
  void Start(vtkAnimationCue::AnimationCueInfo *vtkNotUsed(info));
  void Tick(vtkAnimationCue::AnimationCueInfo *info);
  void End(vtkAnimationCue::AnimationCueInfo *vtkNotUsed(info));
  
protected:
  class AnimationCueObserver : public vtkCommand
  {
  public:
    static AnimationCueObserver *New();
  
    virtual void Execute(vtkObject *vtkNotUsed(caller),
                         unsigned long event,
                         void *calldata);
    AnimationCueObserver();
    ActorAnimator *animator;
  };

  Saccade*               saccade;
  int                    numberOfFrames;
  int                    frameCounter;
  int                    prevFrame;
  vtkActor*              actor;
  AnimationCueObserver*  observer;
  double*                startPosition;
  double*                endPosition;
  double                 speedFactor;
};

class AnimationSceneObserver : public vtkCommand
{
public:
  static AnimationSceneObserver *New();
  
  void SetRenderWindow(vtkRenderWindow *renWin);
  virtual void Execute(vtkObject *vtkNotUsed(caller),
                       unsigned long event,
                       void *vtkNotUsed(calldata));
  
protected:
  AnimationSceneObserver();
  ~AnimationSceneObserver();

  vtkRenderWindow *RenderWindow;
};

#endif
