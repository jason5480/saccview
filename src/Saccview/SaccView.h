#ifndef _SaccView_H_
#define _SaccView_H_

// My staff
#include <saccadesimulator.h>
// Qt
#include <QMainWindow>
#include <QString>
// VTK
#include <vtkSmartPointer.h>

// Forward class declarations
class Ui_MainWindow;
class QStandardItemModel;
class vtkActor;
class vtkRenderer;
class vtkRenderWindow;
class QVTKInteractor;
class vtkAnimationScene;
class vtkOrientationMarkerWidget;

using namespace notremor;

class SaccView : public QMainWindow
{
  Q_OBJECT

public:
  // Constructor/Destructor
  SaccView();
  ~SaccView();
  
private:
  // Methods
  void ReadFile(QString &filename);
  void createTrajectories();

private:
  // Designer form
  Ui_MainWindow *ui;

  // Data members
  QStandardItemModel *model;

  // Simulation
  Eye eyeParams;
  Saccade rightSaccade;
  Saccade leftSaccade;
  QString resultDir;

  // Visualization
  vtkSmartPointer<vtkRenderer> leftRenderer;
  vtkSmartPointer<vtkRenderer> rightRenderer;
  vtkSmartPointer<vtkRenderWindow> renderWin;
  vtkSmartPointer<QVTKInteractor> interactor;
  vtkSmartPointer<vtkActor> rightEye;
  vtkSmartPointer<vtkActor> leftEye;
  vtkSmartPointer<vtkActor> head;
  vtkSmartPointer<vtkActor> brain;
  vtkSmartPointer<vtkActor> medialRectusMuscle;
  vtkSmartPointer<vtkActor> lateralRectusMuscle;
  vtkSmartPointer<vtkActor> inferiorRectusMuscle;
  vtkSmartPointer<vtkActor> superiorRectusMuscle;
  vtkSmartPointer<vtkActor> superiorObliqueMuscle;
  vtkSmartPointer<vtkActor> inferiorObliqueMuscle;
  vtkSmartPointer<vtkActor> eyeTrajectory;
  vtkSmartPointer<vtkActor> planeTrajectory;
  vtkSmartPointer<vtkActor> plane;
  vtkSmartPointer<vtkActor> rightEyeTarget;
  vtkSmartPointer<vtkActor> leftEyeTarget;
  vtkSmartPointer<vtkActor> userTarget;
  vtkSmartPointer<vtkAnimationScene> scene;
  vtkSmartPointer<vtkOrientationMarkerWidget> orientationWidget;

public slots:
  // Slots
  void on_simulatePushButton_clicked();
  void on_playPushButton_clicked();
  void on_halfHeadCheckBox_stateChanged(int state);
  void on_eyeTrajectoryCheckBox_stateChanged(int state);
  void on_planeTrajectoryCheckBox_stateChanged(int state);
  void on_activationsChartPushButton_clicked();
  void on_rotationsChartPushButton_clicked();
  void on_parChartPushButton_clicked();
  void on_pieChartPushButton_clicked();
  void on_barChartPushButton_clicked();
  void on_applyPushButton_clicked();
  void on_savePushButton_clicked();

  //virtual void slotSimulate();
  virtual void slotOpenFile();
  virtual void slotExit();
};

#endif
