// My staff
#include "SaccView.h"
#include "AnimateActors.h"
#include "InteractorStyle.h"
// Qt
// UI generated from Qt
#include "ui_mainwindow.h"
#include <QtCore>
#include <QtGui>
#include <QtXml>
#include <QDir.h>
#include <QFile>
#include <QDebug>
// VTK
// Basic staff
//#include <vtkDataObjectToTable.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkTexture.h>
#include <vtkProperty.h>
#include <vtkAxesActor.h>
#include <vtkCamera.h>
// Collections
#include <vtkRendererCollection.h>
#include <vtkActorCollection.h>
// Readers
#include <vtkOBJReader.h>
#include <vtkJPEGReader.h>
// Source
#include <vtkSphereSource.h>
#include <vtkPlaneSource.h>
// Geometry
#include <vtkPoints.h>
#include <vtkPolyLine.h>
#include <vtkPlane.h>
#include <vtkOBBTree.h>
#include <vtkQuadric.h>
#include <vtkSampleFunction.h>
#include <vtkContourFilter.h>
// Math
#include <vtkMath.h>
#include <vtkTransform.h>
#include <vtkMatrix3x3.h>
#include <vtkMatrix4x4.h>
// Colors
#include <vtkNamedColors.h>
#include <vtkColorTransferFunction.h>
// Data
#include <vtkPolyDataNormals.h>
#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
//#include <vtkParametricSpline.h>
//#include <vtkParametricFunctionSource.h>
#include <vtkSphericalTransform.h>
// Widgets
#include <vtkOrientationMarkerWidget.h>
// Animation
#include <vtkAnimationScene.h>
// Plots
#include <vtkChartXY.h>
#include <vtkChartParallelCoordinates.h>
#include <vtkChartPie.h>
#include <vtkTable.h>
#include <vtkPlot.h>
#include <vtkPlotPoints.h>
#include <vtkPlotPie.h>
#include <vtkIntArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkStringArray.h>
#include <vtkContextView.h>
#include <vtkContextScene.h>
#include <vtkPen.h>
#include <vtkColorSeries.h>
// Macro definition
#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

// Constructor
SaccView::SaccView()
{
  //-------------------------------SET UP QT------------------------------------
  // Set ui from QT Designer
  this->ui = new Ui_MainWindow;
  this->ui->setupUi(this);

  ui->playPushButton->setEnabled(false);
  /*
  QString dirName = QApplication::applicationDirPath() + "/../../config/");
  QDir dirtectory(dirName);
  QString fileName;
  // Check if folder exists and set file name
  if(!dirtectory.exists())
  {
    qDebug() << "Dir " << dirName << " does not exist";
    // Create directory
    dirtectory.mkdir(dirName);
  }
  fileName += "userParams.xml";
  qDebug() << "File " <<fileName;
  
  // Create default config file
  fileName += "defaultParams.xml";
  qDebug() << "File " <<fileName;
  // Write to file
  QFile file(fileName);
  if (!file.exists())
  {
    qDebug() << "File " << fileName << " does not exist";
  }
  else
  {
    qDebug() << "File " << fileName << " exist";
  }
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    qDebug() << "Failed to open the file";
  }
  else 
  {
    QTextStream stream(&file);
    stream << document.toString();
    file.close();
    qDebug() << "Finished";
  }

  // Add some elements


  //*/

  QString configFile = QApplication::applicationDirPath() + "/../../config/";
  configFile += "default_params.txt";
  //QString configFile = QApplication::applicationDirPath() + "/../../config/");
  //configFile += "userParams.xml";
  // Read default parameters
  ReadFile(configFile);

  // Show parameters on ui
  ui->massDoubleSpinBox->setValue(eyeParams.mass);
  ui->radiusDoubleSpinBox->setValue(eyeParams.R);
  ui->rangeDoubleSpinBox->setValue(eyeParams.range);
  ui->fibLenDoubleSpinBox->setValue(eyeParams.optimalFiberLength);
  ui->tenSlacLenDoubleSpinBox->setValue(eyeParams.tendonSlackLength);
  ui->medRecDoubleSpinBox->setValue(eyeParams.muscleParams.force_med_rect);
  ui->latRecDoubleSpinBox->setValue(eyeParams.muscleParams.force_lat_rect);
  ui->infRecDoubleSpinBox->setValue(eyeParams.muscleParams.force_inf_rect);
  ui->supRecDoubleSpinBox->setValue(eyeParams.muscleParams.force_sup_rect);
  ui->supOblDoubleSpinBox->setValue(eyeParams.muscleParams.force_sup_obl);
  ui->infOblDoubleSpinBox->setValue(eyeParams.muscleParams.force_inf_obl);
  ui->stifnessDoubleSpinBox->setValue(eyeParams.passiveStiffness);
  ui->viscosityDoubleSpinBox->setValue(eyeParams.passiveViscosity);

  // Set up action signals and slots
  //connect(this->ui->pushButton, SIGNAL(clicked()), this, SLOT(slotOpenFile()));
  //connect(this->ui->actionOpenFile,SIGNAL(triggered()),this,SLOT(slotOpenFile()));
  //connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
  
  //-------------------------------WED QT/VTK-----------------------------------
  // VTK Renderers
  leftRenderer = vtkSmartPointer<vtkRenderer>::New();
  leftRenderer->SetViewport(0.0, 0.0, 0.5, 1.0);
  rightRenderer = vtkSmartPointer<vtkRenderer>::New();
  rightRenderer->SetViewport(0.5, 0.0, 1.0, 1.0);

  // QVTK Interactor
  interactor = ui->qvtkWidget->GetInteractor();
  vtkSmartPointer<InteractorStyle> style = vtkSmartPointer<InteractorStyle>::New();
  interactor->SetInteractorStyle(style);
  style->SetInteractor(interactor);
  style->SetCurrentRenderer(rightRenderer);

  // VTK/Qt wedded
  renderWin = ui->qvtkWidget->GetRenderWindow();
  renderWin->AddRenderer(leftRenderer);
  renderWin->AddRenderer(rightRenderer);

  //-------------------------------SET UP VTK-----------------------------------
  QString filename;
  // Eyes
  VTK_CREATE(vtkOBJReader, rightEyeReader);
  filename = QApplication::applicationDirPath() + "/../../resources/obj/eye.obj";
  rightEyeReader->SetFileName(filename.toStdString().c_str());
  rightEyeReader->Update();

  VTK_CREATE(vtkPolyDataMapper, rightEyeMapper);
  rightEyeMapper->SetInputConnection(rightEyeReader->GetOutputPort());
  
  VTK_CREATE(vtkJPEGReader, eyeTextureReader);
  filename = QApplication::applicationDirPath() + "/../../resources/tex/eye.jpg";
  eyeTextureReader->SetFileName(filename.toStdString().c_str());
  eyeTextureReader->Update();

  VTK_CREATE(vtkTexture, eyeTexture);
  eyeTexture->SetInputConnection(eyeTextureReader->GetOutputPort());

  rightEye = vtkSmartPointer<vtkActor>::New();
  rightEye->SetMapper(rightEyeMapper);
  rightEye->SetTexture(eyeTexture);
  rightEye->SetScale(eyeParams.R/100.);
  rightEye->SetPosition(eyeParams.eyesDist/2., 0, 0);
  
  VTK_CREATE(vtkOBJReader, leftEyeReader);
  filename = QApplication::applicationDirPath() + "/../../resources/obj/eye.obj";
  leftEyeReader->SetFileName(filename.toStdString().c_str());
  leftEyeReader->Update();

  VTK_CREATE(vtkPolyDataMapper, leftEyeMapper);
  leftEyeMapper->SetInputConnection(leftEyeReader->GetOutputPort());

  leftEye = vtkSmartPointer<vtkActor>::New();
  leftEye->SetMapper(leftEyeMapper);
  leftEye->SetTexture(eyeTexture);
  leftEye->SetScale(eyeParams.R/100.);
  leftEye->SetPosition(-eyeParams.eyesDist/2., 0, 0);

  // Head
  VTK_CREATE(vtkOBJReader, headReader);
  filename = QApplication::applicationDirPath() + "/../../resources/obj/MaleHead.obj";
  headReader->SetFileName(filename.toStdString().c_str());
  headReader->Update();
  
  VTK_CREATE(vtkPolyDataMapper, headMapper);
  headMapper->SetInputConnection(headReader->GetOutputPort());
  
  head = vtkSmartPointer<vtkActor>::New();
  head->SetMapper(headMapper);
  head->SetScale(0.4);
  head->SetPosition(0, -0.012, 0.064);
  head->GetProperty()->SetColor(240./255., 207./255., 180./255.);

  // Brain
  VTK_CREATE(vtkOBJReader, brainReader);
  filename = QApplication::applicationDirPath() + "/../../resources/obj/lh.pial.obj";
  brainReader->SetFileName(filename.toStdString().c_str());
  brainReader->Update();

  VTK_CREATE(vtkPolyDataMapper, brainMapper);
  brainMapper->SetInputConnection(brainReader->GetOutputPort());
  
  brain = vtkSmartPointer<vtkActor>::New();
  brain->SetMapper(brainMapper);
  brain->SetScale(0.001);
  brain->RotateX(-72);
  brain->SetPosition(0.001, 0.051, 0.062);
  brain->GetProperty()->SetColor(0.5, 0.4, 0.6);
  
  // Muscles
  vtkSmartPointer<vtkQuadric> quadric = vtkSmartPointer<vtkQuadric>::New();
  quadric->SetCoefficients(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  // sample the quadric function
  VTK_CREATE(vtkSampleFunction, sample);
  sample->SetSampleDimensions(50, 50, 50);
  sample->SetImplicitFunction(quadric);
  //double xmin = 0, xmax=1, ymin=0, ymax=1, zmin=0, zmax=1;
  double xmin = -5, xmax=5, ymin=-5, ymax=5, zmin=-12, zmax=12;
  sample->SetModelBounds(xmin, xmax, ymin, ymax, zmin, zmax);

  //create the 0 isosurface
  VTK_CREATE(vtkContourFilter, contours);
  contours->SetInputConnection(sample->GetOutputPort());
  contours->GenerateValues(1, 1, 1);

  // map the contours to graphical primitives
  VTK_CREATE(vtkPolyDataMapper, contourMapper);
  contourMapper->SetInputConnection(contours->GetOutputPort());
  contourMapper->SetScalarRange(0.0, 1.2);

  medialRectusMuscle = vtkSmartPointer<vtkActor>::New();
  medialRectusMuscle->SetMapper(contourMapper);
  medialRectusMuscle->SetScale(0.001);
  medialRectusMuscle->SetPosition(eyeParams.eyesDist/2 -eyeParams.R, 0.0, 0.008);
  
  lateralRectusMuscle = vtkSmartPointer<vtkActor>::New();
  lateralRectusMuscle->SetMapper(contourMapper);
  lateralRectusMuscle->SetScale(0.001);
  lateralRectusMuscle->SetPosition(eyeParams.eyesDist/2 + eyeParams.R, 0.0, 0.008);

  inferiorRectusMuscle = vtkSmartPointer<vtkActor>::New();
  inferiorRectusMuscle->SetMapper(contourMapper);
  inferiorRectusMuscle->SetScale(0.001);
  inferiorRectusMuscle->SetPosition(eyeParams.eyesDist/2, -eyeParams.R, 0.008);

  superiorRectusMuscle = vtkSmartPointer<vtkActor>::New();
  superiorRectusMuscle->SetMapper(contourMapper);
  superiorRectusMuscle->SetScale(0.001);
  superiorRectusMuscle->SetPosition(eyeParams.eyesDist/2, eyeParams.R, 0.008);
  
  /*
  // Create three points. We will join (Origin and P0) with a red line and
  //(Origin and P1) with a green line
  double origin[3] = {0.0, 0.0, 0.0};
  double p0[3] = {100.0, 0.0, 0.0};
  double p1[3] = {0.0, 100.0, 0.0};
  double p2[3] = {0.0, 100.0, 200.0};
  double p3[3] = {100.0, 200.0, 300.0};
    
  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points1 = vtkSmartPointer<vtkPoints>::New();
  points1->InsertNextPoint(origin);
  points1->InsertNextPoint(p0);
  points1->InsertNextPoint(p1);

  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points2 = vtkSmartPointer<vtkPoints>::New();
  points2->InsertNextPoint(p0);
  points2->InsertNextPoint(p1);
  points2->InsertNextPoint(p2);

  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points3 = vtkSmartPointer<vtkPoints>::New();
  points3->InsertNextPoint(p1);
  points3->InsertNextPoint(p2);
  points3->InsertNextPoint(p3);

  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points4 = vtkSmartPointer<vtkPoints>::New();
  points4->InsertNextPoint(p2);
  points4->InsertNextPoint(p3);
  points4->InsertNextPoint(origin);

  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points5 = vtkSmartPointer<vtkPoints>::New();
  points5->InsertNextPoint(p3);
  points5->InsertNextPoint(origin);
  points5->InsertNextPoint(p0);

  // Create a vtkPoints object and store the points in it
  vtkSmartPointer<vtkPoints> points6 = vtkSmartPointer<vtkPoints>::New();
  points6->InsertNextPoint(p1);
  points6->InsertNextPoint(p0);
  points6->InsertNextPoint(p2);

  vtkSmartPointer<vtkParametricSpline> spline1 = vtkSmartPointer<vtkParametricSpline>::New();
  spline1->SetPoints(points1);

  vtkSmartPointer<vtkParametricSpline> spline2 = vtkSmartPointer<vtkParametricSpline>::New();
  spline2->SetPoints(points2);

  vtkSmartPointer<vtkParametricSpline> spline3 = vtkSmartPointer<vtkParametricSpline>::New();
  spline3->SetPoints(points3);

  vtkSmartPointer<vtkParametricSpline> spline4 = vtkSmartPointer<vtkParametricSpline>::New();
  spline4->SetPoints(points4);

  vtkSmartPointer<vtkParametricSpline> spline5 = vtkSmartPointer<vtkParametricSpline>::New();
  spline5->SetPoints(points5);
  
  vtkSmartPointer<vtkParametricSpline> spline6 = vtkSmartPointer<vtkParametricSpline>::New();
  spline6->SetPoints(points6);

  vtkSmartPointer<vtkParametricFunctionSource> functionSource1 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource1->SetParametricFunction(spline1);
  functionSource1->Update();

  vtkSmartPointer<vtkParametricFunctionSource> functionSource2 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource2->SetParametricFunction(spline2);
  functionSource2->Update();
  
  vtkSmartPointer<vtkParametricFunctionSource> functionSource3 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource3->SetParametricFunction(spline3);
  functionSource3->Update();
  
  vtkSmartPointer<vtkParametricFunctionSource> functionSource4 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource4->SetParametricFunction(spline4);
  functionSource4->Update();
  
  vtkSmartPointer<vtkParametricFunctionSource> functionSource5 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource5->SetParametricFunction(spline5);
  functionSource5->Update();
  
  vtkSmartPointer<vtkParametricFunctionSource> functionSource6 = vtkSmartPointer<vtkParametricFunctionSource>::New();
  functionSource6->SetParametricFunction(spline6);
  functionSource6->Update();
  //*/

  /*
  VTK_CREATE(vtkPolyDataMapper, mapper1);
  VTK_CREATE(vtkPolyDataMapper, mapper2);
  VTK_CREATE(vtkPolyDataMapper, mapper3);
  VTK_CREATE(vtkPolyDataMapper, mapper4);
  VTK_CREATE(vtkPolyDataMapper, mapper5);
  VTK_CREATE(vtkPolyDataMapper, mapper6);
  mapper1->SetInputConnection(functionSource1->GetOutputPort());
  mapper2->SetInputConnection(functionSource2->GetOutputPort());
  mapper3->SetInputConnection(functionSource3->GetOutputPort());
  mapper4->SetInputConnection(functionSource4->GetOutputPort());
  mapper5->SetInputConnection(functionSource5->GetOutputPort());
  mapper6->SetInputConnection(functionSource6->GetOutputPort());
  //*/

  /*
  VTK_CREATE(vtkActor, muscle1);
  muscle1->SetMapper(mapper1);

  VTK_CREATE(vtkActor, muscle2);
  muscle2->SetMapper(mapper2);
  
  VTK_CREATE(vtkActor, muscle3);
  muscle3->SetMapper(mapper3);
  
  VTK_CREATE(vtkActor, muscle4);
  muscle4->SetMapper(mapper4);
  
  VTK_CREATE(vtkActor, muscle5);
  muscle5->SetMapper(mapper5);
  
  VTK_CREATE(vtkActor, muscle6);
  muscle6->SetMapper(mapper6);
  //*/

  // Plane
  VTK_CREATE(vtkPlaneSource, planeSource);
  planeSource->SetCenter(0.0, 0.0, 0.0);
  planeSource->SetNormal(0.0, 0.0, 1.0);
  planeSource->Update();

  VTK_CREATE(vtkPolyDataMapper, planeMapper);
  planeMapper->SetInputConnection(planeSource->GetOutputPort());
  
  plane = vtkSmartPointer<vtkActor>::New();
  plane->SetMapper(planeMapper);
  plane->SetScale(2);
  plane->SetPosition(0.0, 0.0, -2.0);
  plane->GetProperty()->SetColor(0.7, 0.7, 0.7);

  // Tragectories
  eyeTrajectory   = vtkSmartPointer<vtkActor>::New();
  planeTrajectory = vtkSmartPointer<vtkActor>::New();
  
  // Create Cliping Plane
  VTK_CREATE(vtkPlane, clippingPlane);
  clippingPlane->SetOrigin(plane->GetPosition());
  double *planeOrientation = plane->GetOrientation();
  double n[3] = {0.0, 0.0, 1.0};
  double normal[3];
  
  VTK_CREATE(vtkTransform, transform);
  transform->Identity();
  transform->RotateX(planeOrientation[0]);
  transform->RotateY(planeOrientation[1]);
  transform->RotateZ(planeOrientation[2]);
  transform->TransformPoint(n, normal);
  clippingPlane->SetNormal(normal);

  //std::cout << "PlaneOrigin: [" << plane->GetPosition()[0] << " ," 
  //          << plane->GetPosition()[1] << " ," << plane->GetPosition()[2] 
  //          << "]" << std::endl;
  //std::cout << "PlaneNormal: [" << normal[0] << " ," << normal[1] << " ,"
  //          << normal[2] << "]" << std::endl  << std::endl;
  
  // Right Eye Target
  double *eyePosition    = rightEye->GetPosition();
  double *eyeOrientation = rightEye->GetOrientation();
  
  double pupilDirection[3] = {0.0, 0.0, -1.0};
  double eyeDirection[3];
  
  transform->Identity();
  transform->RotateX(eyeOrientation[0]);
  transform->RotateY(eyeOrientation[1]);
  transform->RotateZ(eyeOrientation[2]);
  transform->TransformPoint(pupilDirection, eyeDirection);

  double point2[3] = {eyePosition[0] + 5*eyeDirection[0],
                      eyePosition[1] + 5*eyeDirection[1],
                      eyePosition[2] + 5*eyeDirection[2]};
  double t=0;
  double resultPosition[3];
  
  std::cout << "RightEyePosition: [" << eyePosition[0] << " ,"
            << eyePosition[1] << " ," << eyePosition[2] 
            << "]" << std::endl;
  std::cout << "RightEyeDirection: [" << eyeDirection[0] << " ," 
            << eyeDirection[1] << " ," << eyeDirection[2] << "]" << std::endl;
  std::cout << "Point2: [" << point2[0] << " ," << point2[1] << " ," 
            << point2[2] << "]" << std::endl;
  
  VTK_CREATE(vtkSphereSource, sphereSource);
  sphereSource->SetRadius(0.01);
  sphereSource->SetCenter(0.0, 0.0, 0.0);

  VTK_CREATE(vtkPolyDataMapper, sphereMapper);
  sphereMapper->SetInputConnection(sphereSource->GetOutputPort());
  
  rightEyeTarget = vtkSmartPointer<vtkActor>::New();
  rightEyeTarget->SetMapper(sphereMapper);
  rightEyeTarget->GetProperty()->SetColor(0.0, 0.0, 1.0);

  if(clippingPlane->IntersectWithLine(eyePosition, point2, t, resultPosition))
  {
    std::cout << "Result: [" << resultPosition[0] << " ," << resultPosition[1] 
              << " ," << resultPosition[2] << "] At: " << t << std::endl 
              << std::endl;
    rightEyeTarget->SetPosition(resultPosition);
  }
  else 
  {
    std::cout << "Paralel line Plane" << std::endl << std::endl;
    rightEyeTarget->SetPosition(0.0, 0.0, 0.0);
  }

  // Left Eye Target
  eyePosition    = leftEye->GetPosition();
  eyeOrientation = leftEye->GetOrientation();
  
  transform->Identity();
  transform->RotateX(eyeOrientation[0]);
  transform->RotateY(eyeOrientation[1]);
  transform->RotateZ(eyeOrientation[2]);
  transform->TransformPoint(pupilDirection, eyeDirection);

  point2[0] = eyePosition[0] + 5*eyeDirection[0];
  point2[1] = eyePosition[1] + 5*eyeDirection[1];
  point2[2] = eyePosition[2] + 5*eyeDirection[2];
  
  std::cout << "LeftEyePosition: [" << eyePosition[0] << " ,"
            << eyePosition[1] << " ," << eyePosition[2] 
            << "]" << std::endl;
  std::cout << "LeftEyeDirection: [" << eyeDirection[0] << " ," 
            << eyeDirection[1] << " ," << eyeDirection[2] << "]" << std::endl;
  std::cout << "Point2: [" << point2[0] << " ," << point2[1] << " ," 
            << point2[2] << "]" << std::endl;
  
  leftEyeTarget = vtkSmartPointer<vtkActor>::New();
  leftEyeTarget->SetMapper(sphereMapper);
  leftEyeTarget->GetProperty()->SetColor(0.0, 1.0, 0.0);

  if(clippingPlane->IntersectWithLine(eyePosition, point2, t, resultPosition))
  {
    std::cout << "Result: [" << resultPosition[0] << " ," << resultPosition[1] 
              << " ," << resultPosition[2] << "] At: " << t << std::endl
              << std::endl;
    leftEyeTarget->SetPosition(resultPosition);
  }
  else 
  {
    std::cout << "Paralel line Plane" << std::endl << std::endl;
    leftEyeTarget->SetPosition(0.0, 0.0, 0.0);
  }
  
  // Set up cameras
  vtkCamera* leftCamera = leftRenderer->GetActiveCamera();
  leftCamera->SetPosition(0.5, 0.0, -0.5);
  leftCamera->SetFocalPoint(0.0, 0.0, 0.0);

  vtkCamera* rightCamera = rightRenderer->GetActiveCamera();
  rightCamera->SetPosition(0, 0.5, 3.0);
  rightCamera->SetFocalPoint(plane->GetCenter());

  // User Target
  //*
  double *camPosition  = rightCamera->GetPosition();
  double *camDirection = rightCamera->GetDirectionOfProjection();
  double *camFocus     = rightCamera->GetFocalPoint();
  double *camView      = rightCamera->GetViewUp();
  double p2[3] = {camPosition[0] + 10*camDirection[0],
                  camPosition[1] + 10*camDirection[1],
                  camPosition[2] + 10*camDirection[2]};
  double result[3];
  
  std::cout << "CamPosition: [" << camPosition[0] << " ," << camPosition[1] 
            << " ," << camPosition[2] << "]" << std::endl;
  std::cout << "CamDirection: [" << camDirection[0] << " ," << camDirection[1] 
            << " ," << camDirection[2] << "]" << std::endl;
  std::cout << "CamFocus: [" << camFocus[0] << " ," << camFocus[1] << " ," 
            << camFocus[2] << "]" << std::endl;
  std::cout << "CamView: [" << camView[0] << " ," << camView[1] << " ," 
            << camView[2] << "]" << std::endl;
  std::cout << "Point2: [" << p2[0] << " ," << p2[1] << " ," << p2[2] << "]" 
            << std::endl;
  
  userTarget = vtkSmartPointer<vtkActor>::New();
  userTarget->SetMapper(sphereMapper);
  userTarget->GetProperty()->SetColor(1.0, 0.0, 0.0);

  if(clippingPlane->IntersectWithLine(camPosition, p2, t, result))
  {
    std::cout << "Result: [" << result[0] << " ," << result[1] << " ," 
              << result[2] << "]" << std::endl << std::endl;
    userTarget->SetPosition(result);
  }
  else 
  {
    std::cout << "Paralel line Plane" << std::endl;
    userTarget->SetPosition(0.0, 0.0, 0.0);
  }
  //*/
  
  // Asign Actors to renderers
  leftRenderer->AddActor(rightEye);
  leftRenderer->AddActor(leftEye);
  leftRenderer->AddActor(head);
  leftRenderer->AddActor(brain);
  leftRenderer->AddActor(lateralRectusMuscle);
  leftRenderer->AddActor(medialRectusMuscle);
  leftRenderer->AddActor(inferiorRectusMuscle);
  leftRenderer->AddActor(superiorRectusMuscle);
  //leftRenderer->AddActor(superiorObliqueMuscle);
  //leftRenderer->AddActor(medialRectusMuscle);
  leftRenderer->AddActor(plane);
  leftRenderer->AddActor(rightEyeTarget);
  leftRenderer->AddActor(leftEyeTarget);
  leftRenderer->AddActor(userTarget);
  leftRenderer->AddActor(eyeTrajectory);
  leftRenderer->AddActor(planeTrajectory);
  leftRenderer->SetBackground(0x33/255., 0x47/255., 0x34/255.);

  vtkActorCollection *actorCollection = leftRenderer->GetActors();
  vtkActor *currentActor;
  actorCollection->InitTraversal();
  for(vtkIdType i = 0; i < actorCollection->GetNumberOfItems(); i++)
  {
    currentActor = actorCollection->GetNextActor();
    rightRenderer->AddActor(currentActor);
  }
  rightRenderer->SetBackground(0.0, 0.0, 0.0);
  
  style->SetPlane(plane);
  style->SetTarget(userTarget);

  //-------------------------------VTK WIDGETS----------------------------------
  // Axes Widget
  //*
  VTK_CREATE(vtkAxesActor, axes);
  orientationWidget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  orientationWidget->SetOutlineColor(0.0, 0.0, 0.0);
  orientationWidget->SetOrientationMarker(axes);
  orientationWidget->SetInteractor(
    vtkRenderWindowInteractor::SafeDownCast(interactor));
  orientationWidget->SetViewport(0.0, 0.0, 0.12, 0.12);
  orientationWidget->SetEnabled(1);
  orientationWidget->InteractiveOff();
  //*/
};

SaccView::~SaccView()
{
  // The smart pointers should clean up for up
}

void SaccView::ReadFile(QString &filename)
{
  //*
  model = new QStandardItemModel(0, 1, this);
  QStandardItem *root = new QStandardItem("Eye");
  model->appendRow(root);

  QDomDocument document;
  
  QString configFile = QApplication::applicationDirPath() + "/../../config/userParams.xml";
  QFile file(configFile);

  if (file.open(QIODevice::ReadOnly | QIODevice::Text))
  {
    int errorLine, errorColumn;
    QString errorMsg;
    QDomDocument document;
    if (!document.setContent(&file, &errorMsg, &errorLine, &errorColumn))
    {
      QString error("Syntax error line %1, column %2:\n%3");
      error = error.arg(errorLine).arg(errorColumn).arg(errorMsg);
      qDebug() << error;
    }
    file.close();

    // Get the xml root
    QDomElement xmlroot = document.firstChildElement();
    QDomNodeList nodes = xmlroot.childNodes();

    // Standard loop
    for (int i = 0; i <nodes.count()-1; i++)
    {
      qDebug();
      QDomElement node = nodes.at(i).toElement();
      qDebug() << node.attribute("ID").toInt() << node.attribute("Name");

      QStandardItem *nodeItem = new QStandardItem(node.attribute("Name"));
      root->appendRow(nodeItem);

    
      QDomNodeList elements = node.childNodes();
      for (int j = 0; j < elements.count(); j++)
      {
        QDomElement element = elements.at(j).toElement();
        qDebug() << "\t" << element.attribute("ID").toInt() 
                 << element.attribute("Name") 
                 << element.attribute("Value").toDouble();

        QList<QStandardItem*> listValues;
        QStandardItem *elementItem = new QStandardItem(element.attribute("Name"));
        QStandardItem *elementItemValue = new QStandardItem(element.attribute("Value"));
        listValues.append(elementItemValue);
        elementItem->insertColumn(0, listValues);
        nodeItem->appendRow(elementItem);
      }
    }
    
    // Epeidi me talaipwrei o Xristos
    QDomElement node = nodes.at(0).toElement();
    QDomNodeList elements = node.childNodes();
    QDomElement element = elements.at(0).toElement();
    eyeParams.mass = element.attribute("Value").toDouble();

    element = elements.at(1).toElement();
    eyeParams.R = element.attribute("Value").toDouble();
    
    element = elements.at(2).toElement();
    eyeParams.range = element.attribute("Value").toDouble();

    
    node = nodes.at(1).toElement();
    elements = node.childNodes();
    element = elements.at(0).toElement();
    eyeParams.passiveStiffness = element.attribute("Value").toDouble();

    element = elements.at(1).toElement();
    eyeParams.passiveViscosity = element.attribute("Value").toDouble();

    eyeParams.eyesDist = 0.051;
    
    // Muscles loop
    QDomElement musclesNode = nodes.at(nodes.count()-1).toElement();
    qDebug() << musclesNode.attribute("ID").toInt() << musclesNode.attribute("Name");

    QStandardItem *musclesItem = new QStandardItem(musclesNode.attribute("Name"));
    root->appendRow(musclesItem);
    

    QDomNodeList muscles = musclesNode.childNodes();
    for (int k = 0; k < muscles.count(); k++)
    {
      QDomElement muscle = muscles.at(k).toElement();
      eyeParams.muscleParams.forces[k] = muscle.attribute("MaxForce").toDouble();
      eyeParams.optimalFiberLength = muscle.attribute("FiberLength").toDouble();
      eyeParams.tendonSlackLength = muscle.attribute("TendonLength").toDouble();
      qDebug() << "\t" << muscle.attribute("ID").toInt() 
               << muscle.attribute("Name") 
               << muscle.attribute("MaxForce").toDouble()
               << muscle.attribute("FiberLength").toDouble()
               << muscle.attribute("TendonLength").toDouble();
      
      QList<QStandardItem*> listValues;
      QStandardItem *muscleItem = new QStandardItem(muscle.attribute("Name"));
      QStandardItem *muscleItemForce = new QStandardItem(muscle.attribute("MaxForce"));
      listValues.append(muscleItemForce);
      QStandardItem *muscleItemFiber = new QStandardItem(muscle.attribute("FiberLength"));
      listValues.append(muscleItemFiber);
      QStandardItem *muscleItemTend = new QStandardItem(muscle.attribute("TendonLength"));
      listValues.append(muscleItemTend);
      muscleItem->insertColumn(0, listValues);
      musclesItem->appendRow(muscleItem);
    }
  }
  else 
  {
    qDebug() << "Error opening the file!!";
    eyeParams.mass = 0.007;
    eyeParams.R = 0.016;
    eyeParams.range = 45;
    eyeParams.eyesDist = 0.051;
    eyeParams.optimalFiberLength = 0.04;
    eyeParams.tendonSlackLength = 0.005;
    eyeParams.passiveStiffness = 0.002;
    eyeParams.passiveViscosity = 0.00012;
    for (int i = 0; i < 6; i++) eyeParams.muscleParams.forces[i] = 20.;
    qDebug() << "Default Params loaded";
  }
  
  ui->treeView->setModel(model);

  /*
  std::list<QString> result;
  int errorLine, errorColumn;
  QString errorMsg;
  QFile modelFile(filename);
  QDomDocument document;
  if (!document.setContent(&modelFile, &errorMsg, &errorLine, &errorColumn))
  {
    QString error("Syntax error line %1, column %2:\n%3");
    error = error.arg(errorLine).arg(errorColumn).arg(errorMsg);
    qDebug() << "Here1";
    //return false;
  }
  else 
    qDebug() << "Here2";
  QDomElement rootElement = document.firstChild().toElement();
  for(QDomNode node = rootElement.firstChild();
      !node.isNull();
      node = node.nextSibling())
  {
      QDomElement element = node.toElement();
      result.push_back(element.tagName());
  }
  //*/
  // Load settings.
  /*
  const Settings conf(filename.toStdString());
  // Create eye param object.
  //Eye eyeParams;
  eyeParams.R = conf.getDbl("eyeRadius");
  eyeParams.mass = conf.getDbl("eyeMass");
  eyeParams.range = conf.getDbl("eyeRange");
  eyeParams.eyesDist = conf.getDbl("eyeDist");
  eyeParams.muscleParams.force_med_rect = conf.getDbl("force_med_rect");
  eyeParams.muscleParams.force_lat_rect = conf.getDbl("force_lat_rect");
  eyeParams.muscleParams.force_inf_rect = conf.getDbl("force_inf_rect");
  eyeParams.muscleParams.force_sup_rect = conf.getDbl("force_sup_rect");
  eyeParams.muscleParams.force_sup_obl = conf.getDbl("force_sup_obl");
  eyeParams.muscleParams.force_inf_obl = conf.getDbl("force_inf_obl");
  eyeParams.optimalFiberLength = conf.getDbl("optimalFiberLength");
  eyeParams.tendonSlackLength = conf.getDbl("tendonSlackLength");
  eyeParams.passiveStiffness = conf.getDbl("passiveStiffness");
  eyeParams.passiveViscosity = conf.getDbl("passiveViscosity");
  //*/
  resultDir = QApplication::applicationDirPath() +
              QString::fromStdString("/../../resultDir");
  //*/
}

void SaccView::createTrajectories()
{
  // Create named colors
  VTK_CREATE(vtkNamedColors, namedColors);
  // Create and Set color transfer function
  VTK_CREATE(vtkColorTransferFunction, colorTransferFunction);
  double *rgb;
  unsigned char col[3];
  rgb = namedColors->GetColor3d("white").GetData();
  colorTransferFunction->AddRGBPoint(0, rgb[0], rgb[1], rgb[2]);
  rgb = namedColors->GetColor3d("red").GetData();
  colorTransferFunction->AddRGBPoint(rightSaccade.timesR.size(), rgb[0], rgb[1], rgb[2]);
  colorTransferFunction->SetColorSpaceToRGB();
  // Create and Set color Array
  VTK_CREATE(vtkUnsignedCharArray, colorsArray);
  colorsArray->SetNumberOfComponents(3);
  colorsArray->SetName("colors");

  // Create a vtkPoints object and store the points in it
  VTK_CREATE(vtkPoints, eyeTrajectoryPoints);

  double pupilDirection[3] = {0.0, 0.0, -1.0};
  double v[3];
  
  for (size_t i=0, n=rightSaccade.timesR.size(); i<n; i++)
  {
    VTK_CREATE(vtkTransform, transform);
    transform->RotateX(rightSaccade.rotRx[i]);
    transform->RotateY(rightSaccade.rotRy[i]);
    transform->RotateZ(rightSaccade.rotRz[i]);
    transform->TransformPoint(pupilDirection, v);
    eyeTrajectoryPoints->InsertNextPoint(v);
    rgb = colorTransferFunction->GetColor(i);
    for (unsigned j = 0; j < 3; j++) col[j] = static_cast<unsigned char>(255.0 * rgb[j]);
    colorsArray->InsertNextTupleValue(col);
  }

  VTK_CREATE(vtkPolyLine, eyeTrajectoryLine);
  eyeTrajectoryLine->GetPointIds()->SetNumberOfIds(rightSaccade.timesR.size());
  for (size_t i=0, n=rightSaccade.timesR.size(); i<n; i++) {
    eyeTrajectoryLine->GetPointIds()->SetId(i,i);
  }
  // Create a cell array to store the lines in and add the lines to it
  VTK_CREATE(vtkCellArray, eyeTrajectoryCells);
  eyeTrajectoryCells->InsertNextCell(eyeTrajectoryLine);

   // Create a polydata to store everything in
  VTK_CREATE(vtkPolyData, eyeTrajectoryPolyData);
  eyeTrajectoryPolyData->SetPoints(eyeTrajectoryPoints);
 
  // Add the lines to the dataset
  eyeTrajectoryPolyData->SetLines(eyeTrajectoryCells);
  eyeTrajectoryPolyData->GetPointData()->SetScalars(colorsArray);

  VTK_CREATE(vtkPolyDataMapper, eyeTrajectoryMapper);
  eyeTrajectoryMapper->SetInputData(eyeTrajectoryPolyData);

  eyeTrajectory->SetMapper(eyeTrajectoryMapper);
  eyeTrajectory->SetScale(eyeParams.R+0.0005);
  eyeTrajectory->SetPosition(eyeParams.eyesDist/2., 0, 0);
  eyeTrajectory->SetVisibility(false);

  // Create a vtkPoints object and store the points in it
  VTK_CREATE(vtkPoints, planeTrajectoryPoints);

  for (size_t i=0, n=rightSaccade.timesR.size(); i<n; i++)
  {
    VTK_CREATE(vtkTransform, transform);
    transform->RotateX(rightSaccade.rotRx[i]);
    transform->RotateY(rightSaccade.rotRy[i]);
    transform->RotateZ(rightSaccade.rotRz[i]);
    transform->TransformPoint(pupilDirection, v);
    planeTrajectoryPoints->InsertNextPoint(v);
    rgb = colorTransferFunction->GetColor(i);
    for (unsigned j = 0; j < 3; j++) col[j] = static_cast<unsigned char>(255.0 * rgb[j]);
    colorsArray->InsertNextTupleValue(col);
  }

  VTK_CREATE(vtkPolyLine, planeTrajectoryLine);
  planeTrajectoryLine->GetPointIds()->SetNumberOfIds(rightSaccade.timesR.size());
  for (size_t i=0, n=rightSaccade.timesR.size(); i<n; i++) {
    planeTrajectoryLine->GetPointIds()->SetId(i,i);
  }
  // Create a cell array to store the lines in and add the lines to it
  VTK_CREATE(vtkCellArray, planeTrajectoryCells);
  planeTrajectoryCells->InsertNextCell(planeTrajectoryLine);

   // Create a polydata to store everything in
  VTK_CREATE(vtkPolyData, planeTrajectoryPolyData);
  planeTrajectoryPolyData->SetPoints(planeTrajectoryPoints);
 
  // Add the lines to the dataset
  planeTrajectoryPolyData->SetLines(planeTrajectoryCells);
  planeTrajectoryPolyData->GetPointData()->SetScalars(colorsArray);

  VTK_CREATE(vtkPolyDataMapper, planeTrajectoryMapper);
  planeTrajectoryMapper->SetInputData(planeTrajectoryPolyData);

  planeTrajectory->SetMapper(planeTrajectoryMapper);
  planeTrajectory->SetScale(eyeParams.R+0.0005);
  planeTrajectory->SetPosition(eyeParams.eyesDist/2., 0, 0);
  planeTrajectory->SetVisibility(ui->eyeTrajectoryCheckBox->isChecked());

  /*
  double startPoint[3];
  VTK_CREATE(vtkTransform, transformStart);
  transformStart->RotateX(rightSaccade.rotRx[0]);
  transformStart->RotateY(rightSaccade.rotRy[0]);
  transformStart->RotateZ(rightSaccade.rotRz[0]);
  transformStart->TransformPoint(pupil, startPoint);
  double endPoint[3];
  VTK_CREATE(vtkTransform, transformEnd);
  transformEnd->RotateX(rightSaccade.rotRx[rightSaccade.rotRx.size()-1]);
  transformEnd->RotateY(rightSaccade.rotRy[rightSaccade.rotRx.size()-1]);
  transformEnd->RotateZ(rightSaccade.rotRz[rightSaccade.rotRx.size()-1]);
  transformEnd->TransformPoint(pupil, endPoint);

  qDebug() << "Last point: [" << endPoint[0] << ", " << endPoint[1] << ", " << endPoint[2] << "]";
  // Create spheres for start and end point
  VTK_CREATE(vtkSphereSource, sphereStartSource);
  sphereStartSource->SetCenter(startPoint);
  sphereStartSource->SetRadius(0.05);
  VTK_CREATE(vtkPolyDataMapper, sphereStartMapper);
  sphereStartMapper->SetInputConnection(sphereStartSource->GetOutputPort());
  VTK_CREATE(vtkActor, sphereStart);
  sphereStart->SetMapper(sphereStartMapper);
  sphereStart->SetScale(eyeParams.R+0.0005);
  sphereStart->SetPosition(eyeParams.eyesDist/2., 0, 0);
  sphereStart->GetProperty()->SetColor(namedColors->GetColor3d("blue").GetData());
  
  VTK_CREATE(vtkSphereSource, sphereEndSource);
  sphereEndSource->SetCenter(endPoint);
  sphereEndSource->SetRadius(0.05);
  VTK_CREATE(vtkPolyDataMapper, sphereEndMapper);
  sphereEndMapper->SetInputConnection(sphereEndSource->GetOutputPort());
  VTK_CREATE(vtkActor, sphereEnd);
  sphereEnd->SetMapper(sphereEndMapper);
  sphereEnd->SetScale(eyeParams.R+0.0005);
  sphereEnd->SetPosition(eyeParams.eyesDist/2., 0, 0);
  sphereEnd->GetProperty()->SetColor(namedColors->GetColor3d("red").GetData());

  renderer->AddActor(sphereStart);
  renderer->AddActor(sphereEnd);
  //*/
  renderWin->Render();
}

// Slots
void SaccView::on_simulatePushButton_clicked()
{
  ui->playPushButton->setEnabled(false);

  double *rightEyePosition = rightEye->GetPosition();
  double *leftEyePosition = leftEye->GetPosition();
  double *targetPosition = userTarget->GetPosition();
  double rightEyeDirection[3] = {targetPosition[0] - rightEyePosition[0],
                                 targetPosition[1] - rightEyePosition[1],
                                 targetPosition[2] - rightEyePosition[2]};
  vtkMath::Normalize(rightEyeDirection);
  double leftEyeDirection[3] = {targetPosition[0] - leftEyePosition[0],
                                targetPosition[1] - leftEyePosition[1],
                                targetPosition[2] - leftEyePosition[2]};
  vtkMath::Normalize(leftEyeDirection);

  double *rightEyeInitialOrientation = rightEye->GetOrientation();
  double *leftEyeInitialOrientation  = leftEye->GetOrientation();
  double rightEyeFinalOrientation[3] = {10, 10, 10};
  double leftEyeFinalOrientation[3]  = {10, 10, 10};
  double pupilDirection[3] = {0.0, 0.0, -1.0};
  //*
  VTK_CREATE(vtkMatrix4x4, matrix);
  matrix->SetElement(0,2,-leftEyeDirection[0]);
  matrix->SetElement(1,2,-leftEyeDirection[1]);
  matrix->SetElement(2,2,-leftEyeDirection[2]);
  VTK_CREATE(vtkTransform, transform);
  transform->Identity();
  transform->SetMatrix(matrix);
  transform->GetOrientation(rightEyeFinalOrientation);
  //*/

  // Read initial conditions and construct rightSaccade object
  unsigned size = sizeof(rightSaccade.rotFromR);
  memcpy(rightSaccade.rotFromR, rightEyeInitialOrientation, size);
  memcpy(rightSaccade.rotToR,   rightEyeFinalOrientation,   size);
  memcpy(rightSaccade.rotFromL, leftEyeInitialOrientation,  size);
  memcpy(rightSaccade.rotToL,   leftEyeFinalOrientation,    size);

  // Perform simulation
  SaccadeSimulator SaccadeSimulator(eyeParams, resultDir.toStdString());
  SaccadeSimulator.simulate(rightSaccade, false);
  
  qDebug() << "Right Eye Direction [" << rightEyeDirection[0] << ", " 
           << rightEyeDirection[1] << ", " << rightEyeDirection[2] << "]";
  qDebug() << "Create simulation from [" << rightEyeInitialOrientation[0]  
           << ", " << rightEyeInitialOrientation[1] 
           << ", " << rightEyeInitialOrientation[2] << "] to [" 
           << rightEyeFinalOrientation[0]  << ", " 
           << rightEyeFinalOrientation[1] << ", " 
           << rightEyeFinalOrientation[2] << "]";

  createTrajectories();

  ui->playPushButton->setEnabled(true);
}

void SaccView::on_playPushButton_clicked()
{
  double speedFactor = ui->speedDoubleSpinBox->value();

  scene = vtkSmartPointer<vtkAnimationScene>::New();
  scene->SetModeToRealTime();
  scene->SetLoop(0);
  scene->SetStartTime(0);
  scene->SetEndTime(0.2+rightSaccade.timesR[rightSaccade.timesR.size()-1]/speedFactor);

  VTK_CREATE(AnimationSceneObserver, sceneObserver);
  sceneObserver->SetRenderWindow(renderWin);
  scene->AddObserver(vtkCommand::AnimationCueTickEvent,sceneObserver);

  // Create an Animation Cue for each actor
  VTK_CREATE(vtkAnimationCue, cue1);
  cue1->SetTimeModeToRelative();
  cue1->SetStartTime(0);
  cue1->SetEndTime(0.1+rightSaccade.timesR[rightSaccade.timesR.size()-1]/speedFactor);
  scene->AddCue(cue1);
  //*
  VTK_CREATE(vtkAnimationCue, cue2);
  cue2->SetTimeModeToRelative();
  cue2->SetStartTime(0);
  cue2->SetEndTime(0.1+rightSaccade.timesR[rightSaccade.timesR.size()-1]/speedFactor);
  scene->AddCue(cue2);
  //*/
  // Create an ActorAnimator for each actor;
  ActorAnimator animateRightEye;
  animateRightEye.SetSpeedFactor(speedFactor);
  animateRightEye.SetSacccade(&rightSaccade);
  animateRightEye.SetActor(rightEye);
  animateRightEye.AddObserversToCue(cue1);
  //*
  ActorAnimator animateLeftEye;
  animateLeftEye.SetSpeedFactor(speedFactor);
  animateLeftEye.SetSacccade(&rightSaccade);
  animateLeftEye.SetActor(leftEye);
  animateLeftEye.AddObserversToCue(cue2);
  //*/
  /*
  std::vector<double> startPosition(3);
    startPosition[0] = eyeParams.eyesDist/2.;
    startPosition[1] = 0;
    startPosition[2] = 0;
  animateRightEye.SetStartPosition(startPosition);
  std::vector<double> endPosition(3);
    endPosition[0] = eyeParams.eyesDist/2.;
    endPosition[1] = eyeParams.eyesDist/2;
    endPosition[2] = 0;
  animateRightEye.SetEndPosition(endPosition);
  //*/
  scene->Play();
  scene->Stop();
}

void SaccView::on_halfHeadCheckBox_stateChanged(int state)
{
  VTK_CREATE(vtkOBJReader, headReader);
  QString filename;
  if (state)
    filename = QApplication::applicationDirPath() +
               "/../../resources/obj/HalfMaleHead.obj";
  else
    filename = QApplication::applicationDirPath() +
               "/../../resources/obj/MaleHead.obj";
  headReader->SetFileName(filename.toStdString().c_str());
  headReader->Update();

  VTK_CREATE(vtkPolyDataMapper, headMapper);
  headMapper->SetInputConnection(headReader->GetOutputPort());

  head->SetMapper(headMapper);
  
  // Rerender window
  renderWin->Render();
}

void SaccView::on_eyeTrajectoryCheckBox_stateChanged(int state)
{
  eyeTrajectory->SetVisibility(state);
  
  // Rerender window
  renderWin->Render();
}

void SaccView::on_planeTrajectoryCheckBox_stateChanged(int state)
{
  planeTrajectory->SetVisibility(state);
  
  // Rerender window
  renderWin->Render();
}

void SaccView::on_activationsChartPushButton_clicked()
{
  // Create a table with some points in it
  VTK_CREATE(vtkTable, table);

  VTK_CREATE(vtkFloatArray, arrT);
  arrT->SetName("TimeAct Axis");
  table->AddColumn(arrT);
 
  VTK_CREATE(vtkFloatArray, arrMR);
  arrMR->SetName("MedialRectus");
  table->AddColumn(arrMR);

  VTK_CREATE(vtkFloatArray, arrLR);
  arrLR->SetName("LateralRectus");
  table->AddColumn(arrLR);
  
  VTK_CREATE(vtkFloatArray, arrIR);
  arrIR->SetName("InferiorRectus");
  table->AddColumn(arrIR);

  VTK_CREATE(vtkFloatArray, arrSR);
  arrSR->SetName("SuperiorRectus");
  table->AddColumn(arrSR);

  VTK_CREATE(vtkFloatArray, arrSO);
  arrSO->SetName("SuperiorOblique");
  table->AddColumn(arrSO);

  VTK_CREATE(vtkFloatArray, arrIO);
  arrIO->SetName("InferiorOblique");
  table->AddColumn(arrIO);

  // Fill in the table with some example values
  int numPoints = rightSaccade.timesActR.size();
  table->SetNumberOfRows(numPoints);
  for (int i = 0; i < numPoints; ++i)
  {
    table->SetValue(i, 0, rightSaccade.timesActR[i]*100);
    table->SetValue(i, 1, rightSaccade.activationsR[i].act_medialRectus);
    table->SetValue(i, 2, rightSaccade.activationsR[i].act_lateralRectus);
    table->SetValue(i, 3, rightSaccade.activationsR[i].act_inferiorRectus);
    table->SetValue(i, 4, rightSaccade.activationsR[i].act_superiorRectus);
    table->SetValue(i, 5, rightSaccade.activationsR[i].act_superiorOblique);
    table->SetValue(i, 6, rightSaccade.activationsR[i].act_inferiorOblique);
  }
 
  // Set up the view
  VTK_CREATE(vtkContextView, view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
 
  // Add multiple line plots, setting the colors etc
  VTK_CREATE(vtkChartXY, chart);
  view->GetScene()->AddItem(chart);

  vtkPlot *line = chart->AddPlot(vtkChart::LINE);
  line->SetInputData(table, 0, 1);
  line->SetColor(255, 0, 0, 255);
  line->SetWidth(2.0);

  line = chart->AddPlot(vtkChart::LINE);
  line->SetInputData(table, 0, 2);
  line->SetColor(0, 255, 0, 255);
  line->SetWidth(3.0);

  line = chart->AddPlot(vtkChart::LINE);
  line->SetInputData(table, 0, 3);
  line->SetColor(0, 0, 255, 255);
  line->SetWidth(1.0);

  line->SetInputData(table, 0, 4);
  line->SetColor(125, 125, 0, 255);
  line->SetWidth(2.0);

  line = chart->AddPlot(vtkChart::LINE);
  line->SetInputData(table, 0, 5);
  line->SetColor(0, 125, 125, 255);
  line->SetWidth(3.0);

  line = chart->AddPlot(vtkChart::LINE);
  line->SetInputData(table, 0, 6);
  line->SetColor(125, 0, 125, 255);
  line->SetWidth(1.0);

  //*/
  //view->GetRenderWindow()->SetMultiSamples(0);
 
  // Start interactor
  view->GetInteractor()->Initialize();
  view->GetInteractor()->Start();
}

void SaccView::on_rotationsChartPushButton_clicked()
{
  //*
  // Create a table with some points in it
  VTK_CREATE(vtkTable, table);

  VTK_CREATE(vtkFloatArray, arrT);
  arrT->SetName("Time Axis");
  table->AddColumn(arrT);
 
  VTK_CREATE(vtkFloatArray, arrX);
  arrX->SetName("RotX");
  table->AddColumn(arrX);
 
  VTK_CREATE(vtkFloatArray, arrY);
  arrY->SetName("RotY");
  table->AddColumn(arrY);
  
  VTK_CREATE(vtkFloatArray, arrZ);
  arrZ->SetName("RotZ");
  table->AddColumn(arrZ);
 
  // Fill in the table with some example values
  int numPoints = rightSaccade.timesR.size();
  table->SetNumberOfRows(numPoints);
  for (int i = 0; i < numPoints; ++i)
  {
    table->SetValue(i, 0, rightSaccade.timesR[i]*100);
    table->SetValue(i, 1, rightSaccade.rotRx[i]);
    table->SetValue(i, 2, rightSaccade.rotRy[i]);
    table->SetValue(i, 3, rightSaccade.rotRz[i]);
  }
 
  // Set up the view
  VTK_CREATE(vtkContextView, view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetRenderWindow()->SetSize(400, 300);
 
  // Add multiple line plots, setting the colors etc
  VTK_CREATE(vtkChartXY, chart);
  view->GetScene()->AddItem(chart);

  vtkPlot *line = chart->AddPlot(vtkChart::POINTS);
  line->SetInputData(table, 0, 1);
  line->SetColor(255, 0, 0, 255);
  line->SetWidth(1.0);
  vtkPlotPoints::SafeDownCast(line)->SetMarkerStyle(vtkPlotPoints::CROSS);

  line = chart->AddPlot(vtkChart::POINTS);
  line->SetInputData(table, 0, 2);
  line->SetColor(0, 255, 0, 255);
  line->SetWidth(1.0);
  vtkPlotPoints::SafeDownCast(line)->SetMarkerStyle(vtkPlotPoints::PLUS);

  line = chart->AddPlot(vtkChart::POINTS);
  line->SetInputData(table, 0, 3);
  line->SetColor(0, 0, 255, 255);
  line->SetWidth(1.0);
  vtkPlotPoints::SafeDownCast(line)->SetMarkerStyle(vtkPlotPoints::CIRCLE);
  
  // Start interactor
  view->GetRenderWindow()->SetMultiSamples(0);
  view->GetInteractor()->Initialize();
  view->GetInteractor()->Start();
  //*/
}

void SaccView::on_parChartPushButton_clicked()
{
  //*
  // Create a table with some points in it
  VTK_CREATE(vtkTable, table);

  VTK_CREATE(vtkFloatArray, arrT);
  arrT->SetName("Time Axis");
  table->AddColumn(arrT);
 
  VTK_CREATE(vtkFloatArray, arrX);
  arrX->SetName("RotX");
  table->AddColumn(arrX);
 
  VTK_CREATE(vtkFloatArray, arrY);
  arrY->SetName("RotY");
  table->AddColumn(arrY);
  
  VTK_CREATE(vtkFloatArray, arrZ);
  arrZ->SetName("RotZ");
  table->AddColumn(arrZ);
 
  // Fill in the table with some example values
  int numPoints = rightSaccade.timesR.size();
  table->SetNumberOfRows(numPoints);
  for (int i = 0; i < numPoints; ++i)
  {
    table->SetValue(i, 0, rightSaccade.timesR[i]*100);
    table->SetValue(i, 1, rightSaccade.rotRx[i]);
    table->SetValue(i, 2, rightSaccade.rotRy[i]);
    table->SetValue(i, 3, rightSaccade.rotRz[i]);
  }
 
  // Set up the view
  VTK_CREATE(vtkContextView, view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetRenderWindow()->SetSize(400, 300);
 
  // Add multiple line plots, setting the colors etc
  VTK_CREATE(vtkChartParallelCoordinates, chart);
  view->GetScene()->AddItem(chart);
  
  chart->GetPlot(0)->SetInputData(table);

  // Start interactor
  view->GetRenderWindow()->SetMultiSamples(0);
  view->GetInteractor()->Initialize();
  view->GetInteractor()->Start();
  //*/
}

void SaccView::on_pieChartPushButton_clicked()
{
  int data[] = {77938,9109,2070,12806,19514};
  char *labels[] = {"Neurons","Nerves","Muscles","Bones","Tisues"};
  //*
  // Create a table with some points in it
  VTK_CREATE(vtkTable, table);

  vtkSmartPointer<vtkIntArray> arrData =
    vtkSmartPointer<vtkIntArray>::New();
  vtkSmartPointer<vtkStringArray> labelArray =
    vtkSmartPointer<vtkStringArray>::New();

  arrData->SetName("Rotations");
  for (int i = 0; i < 5; i++)
  {
    arrData->InsertNextValue(data[i]);
    labelArray->InsertNextValue(labels[i]);
  }
  table->AddColumn(arrData);
 
  // Create a color series to use with our stacks.
  vtkSmartPointer<vtkColorSeries> colorSeries =
    vtkSmartPointer<vtkColorSeries>::New();
  colorSeries->SetColorScheme(vtkColorSeries::BLUES);

  // Set up the view
  VTK_CREATE(vtkContextView, view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetRenderWindow()->SetSize(400, 300);
 
  // Add multiple line plots, setting the colors etc
  VTK_CREATE(vtkChartPie, chart);
  view->GetScene()->AddItem(chart);
  
 // Add multiple line plots, setting the colors etc
  vtkPlotPie *pie = vtkPlotPie::SafeDownCast(chart->AddPlot(0));
  pie->SetColorSeries(colorSeries);
  pie->SetInputData(table);
  pie->SetInputArray(0,"Rotations");
  pie->SetLabels(labelArray);

  chart->SetShowLegend(true);
  chart->SetTitle("Rotations");

  // Start interactor
  view->GetRenderWindow()->SetMultiSamples(0);
  view->GetInteractor()->Initialize();
  view->GetInteractor()->Start();
  //*/
}

void SaccView::on_barChartPushButton_clicked()
{
  //*
  // Create a table with some points in it
  VTK_CREATE(vtkTable, table);

  VTK_CREATE(vtkFloatArray, arrT);
  arrT->SetName("Time Axis");
  table->AddColumn(arrT);
 
  VTK_CREATE(vtkFloatArray, arrX);
  arrX->SetName("RotX");
  table->AddColumn(arrX);
 
  VTK_CREATE(vtkFloatArray, arrY);
  arrY->SetName("RotY");
  table->AddColumn(arrY);
  
  VTK_CREATE(vtkFloatArray, arrZ);
  arrZ->SetName("RotZ");
  table->AddColumn(arrZ);
 
  // Fill in the table with some example values
  int numPoints = rightSaccade.timesR.size();
  table->SetNumberOfRows(numPoints);
  for (int i = 0; i < numPoints; ++i)
  {
    table->SetValue(i, 0, rightSaccade.timesR[i]*100);
    table->SetValue(i, 1, rightSaccade.rotRx[i]);
    table->SetValue(i, 2, rightSaccade.rotRy[i]);
    table->SetValue(i, 3, rightSaccade.rotRz[i]);
  }
 
  // Set up the view
  VTK_CREATE(vtkContextView, view);
  view->GetRenderer()->SetBackground(1.0, 1.0, 1.0);
  view->GetRenderWindow()->SetSize(400, 300);
 
  // Add multiple line plots, setting the colors etc
  VTK_CREATE(vtkChartXY, chart);
  view->GetScene()->AddItem(chart);

  vtkPlot *line = chart->AddPlot(vtkChart::STACKED);
  line->SetInputData(table, 0, 1);
  line->SetColor(255, 0, 0, 255);
  line->SetWidth(1.0);

  line = chart->AddPlot(vtkChart::STACKED);
  line->SetInputData(table, 0, 2);
  line->SetColor(0, 255, 0, 255);
  line->SetWidth(1.0);

  line = chart->AddPlot(vtkChart::STACKED);
  line->SetInputData(table, 0, 3);
  line->SetColor(0, 0, 255, 255);
  line->SetWidth(1.0);
  
  // Start interactor
  view->GetRenderWindow()->SetMultiSamples(0);
  view->GetInteractor()->Initialize();
  view->GetInteractor()->Start();
  //*/
}

void SaccView::on_applyPushButton_clicked()
{
  //*
  eyeParams.mass = ui->massDoubleSpinBox->value();
  eyeParams.R = ui->radiusDoubleSpinBox->value();
  eyeParams.range = ui->rangeDoubleSpinBox->value();
  eyeParams.optimalFiberLength = ui->fibLenDoubleSpinBox->value();
  eyeParams.tendonSlackLength = ui->tenSlacLenDoubleSpinBox->value();
  eyeParams.muscleParams.force_med_rect = ui->medRecDoubleSpinBox->value();
  eyeParams.muscleParams.force_lat_rect = ui->latRecDoubleSpinBox->value();
  eyeParams.muscleParams.force_inf_rect = ui->infRecDoubleSpinBox->value();
  eyeParams.muscleParams.force_sup_rect = ui->supRecDoubleSpinBox->value();
  eyeParams.muscleParams.force_sup_obl  = ui->supOblDoubleSpinBox->value();
  eyeParams.muscleParams.force_inf_obl  = ui->infOblDoubleSpinBox->value();
  eyeParams.passiveStiffness = ui->stifnessDoubleSpinBox->value();
  eyeParams.passiveViscosity = ui->viscosityDoubleSpinBox->value();
  //*/
}

void SaccView::on_savePushButton_clicked()
{
  QString dirName = QApplication::applicationDirPath() + "/../../config/";

  QDir dirtectory(dirName);
  // Check if folder exists and set file name
  if(!dirtectory.exists())
  {
    qDebug() << "Dir " << dirName << " does not exist";
    // Create directory
    dirtectory.mkdir(dirName);
  }

  QString fileName = dirName + "userParams.xml";
  qDebug() << "File " << fileName << " is writing...";
  
  // Write XML
  QDomDocument document;
  // Make the root element
  QDomElement root = document.createElement("Eye");
  // Add it to the document
  document.appendChild(root);
  // Add some elements
  QDomElement anatomyNode = document.createElement("Anatomy");
  anatomyNode.setAttribute("Name", "Anatomy");
  anatomyNode.setAttribute("ID", 0);


  QDomElement massNode = document.createElement("Mass");
  massNode.setAttribute("Name", "Mass");
  massNode.setAttribute("ID", 0);
  massNode.setAttribute("Value", eyeParams.mass);
  anatomyNode.appendChild(massNode);

  QDomElement radiusNode = document.createElement("Radius");
  radiusNode.setAttribute("Name", "Radius");
  radiusNode.setAttribute("ID", 1);
  radiusNode.setAttribute("Value", eyeParams.R);
  anatomyNode.appendChild(radiusNode);

  QDomElement rangeNode = document.createElement("Range");
  rangeNode.setAttribute("Name", "Range");
  rangeNode.setAttribute("ID", 2);
  rangeNode.setAttribute("Value", eyeParams.range);
  anatomyNode.appendChild(rangeNode);

  root.appendChild(anatomyNode);

  /*
  QDomElement lengthNode = document.createElement("Length");
  lengthNode.setAttribute("Name", "Length");
  lengthNode.setAttribute("ID", 1);

  QDomElement fiberLenNode = document.createElement("OptimalFiberLen");
  fiberLenNode.setAttribute("Value", eyeParams.optimalFiberLength);
  fiberLenNode.setAttribute("Name", "OptimalFiberLength");
  lengthNode.appendChild(fiberLenNode);

  QDomElement tenSlacLenNode = document.createElement("TendonSlacLen");
  tenSlacLenNode.setAttribute("Value", eyeParams.tendonSlackLength);
  tenSlacLenNode.setAttribute("Name", "TendonSlackLength");
  lengthNode.appendChild(tenSlacLenNode);

  root.appendChild(lengthNode);
  //*/

  QDomElement passiveNode = document.createElement("PassiveParams");
  passiveNode.setAttribute("Name", "PassiveParams");
  passiveNode.setAttribute("ID", 1);

  QDomElement stiffnessNode = document.createElement("Stiffness");
  stiffnessNode.setAttribute("Name", "Stiffness");
  stiffnessNode.setAttribute("ID", 0);
  stiffnessNode.setAttribute("Value", eyeParams.passiveStiffness);
  passiveNode.appendChild(stiffnessNode);

  QDomElement viscosityNode = document.createElement("Viscosity");
  viscosityNode.setAttribute("Name", "Viscosity");
  viscosityNode.setAttribute("ID", 1);
  viscosityNode.setAttribute("Value", eyeParams.passiveViscosity);
  passiveNode.appendChild(viscosityNode);

  root.appendChild(passiveNode);


  QDomElement muscleNode = document.createElement("Muscles");
  muscleNode.setAttribute("Name", "Muscles");
  muscleNode.setAttribute("ID", 2);
  
  QDomElement medRecNode = document.createElement("MedialRectus");
  medRecNode.setAttribute("Name", "MedialRectus");
  medRecNode.setAttribute("ID", 0);
  medRecNode.setAttribute("MaxForce", eyeParams.muscleParams.force_med_rect);
  medRecNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  medRecNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(medRecNode);

  QDomElement latRecNode = document.createElement("LateralRectus");
  latRecNode.setAttribute("Name", "LateralRectus");
  latRecNode.setAttribute("ID", 1);
  latRecNode.setAttribute("MaxForce", eyeParams.muscleParams.force_lat_rect);
  latRecNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  latRecNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(latRecNode);
  
  QDomElement infRecNode = document.createElement("InferiorRectus");
  infRecNode.setAttribute("Name", "InferiorRectus");
  infRecNode.setAttribute("ID", 2);
  infRecNode.setAttribute("MaxForce", eyeParams.muscleParams.force_inf_rect);
  infRecNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  infRecNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(infRecNode);
  
  QDomElement supRecNode = document.createElement("SuperiorRectus");
  supRecNode.setAttribute("Name", "SuperiorRectus");
  supRecNode.setAttribute("ID", 3);
  supRecNode.setAttribute("MaxForce", eyeParams.muscleParams.force_med_rect);
  supRecNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  supRecNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(supRecNode);

  QDomElement supOblNode = document.createElement("SuperiorOblique");
  supOblNode.setAttribute("Name", "SuperiorOblique");
  supOblNode.setAttribute("ID", 4);
  supOblNode.setAttribute("MaxForce", eyeParams.muscleParams.force_sup_obl);
  supOblNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  supOblNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(supOblNode);
  
  QDomElement infOblNode = document.createElement("InferiorOblique");
  infOblNode.setAttribute("Name", "InferiorOblique");
  infOblNode.setAttribute("ID", 5);
  infOblNode.setAttribute("MaxForce", eyeParams.muscleParams.force_inf_obl);
  infOblNode.setAttribute("FiberLength", eyeParams.optimalFiberLength);
  infOblNode.setAttribute("TendonLength", eyeParams.tendonSlackLength);
  muscleNode.appendChild(infOblNode);

  root.appendChild(muscleNode);
  


  QFile file(fileName);
  if (!file.exists())
  {
    qDebug() << "File " << fileName << " does not exist";
  }
  else
  {
    qDebug() << "File " << fileName << "already exist";
  }

  if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
  {
    qDebug() << "Failed to open the file";
  }
  else 
  {
    qDebug() << "File opened successfully";
    QTextStream stream(&file);
    stream << document.toString();
    file.close();
    qDebug() << "Finished writing";
  }
  qDebug() << "Finished";
}

void SaccView::slotOpenFile()
{
  qDebug() << "Open";
}

void SaccView::slotExit()
{
  qApp->exit();
}
