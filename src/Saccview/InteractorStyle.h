#ifndef _InteractorStyle_H_

#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>

class vtkActor;

class InteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
  static InteractorStyle* New();
  vtkTypeMacro(InteractorStyle, vtkInteractorStyleTrackballCamera);

  void OnChar();

  void SetPlane(vtkSmartPointer<vtkActor> pl);
  void SetTarget(vtkSmartPointer<vtkActor> tar);

private:
  vtkSmartPointer<vtkActor> plane;
  vtkSmartPointer<vtkActor> target;
};

#endif