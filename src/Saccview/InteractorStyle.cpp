#include <InteractorStyle.h>
#include <vtkObjectFactory.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkPlane.h>
#include <vtkPlaneSource.h>
#include <vtkSphereSource.h>
#include <vtkPolyDataMapper.h>
#include <QVTKInteractor.h>

#include "vtkAssemblyPath.h"
#include "vtkProperty.h"
#include <vtkWorldPointPicker.h>
#include <vtkTransform.h>

#include <qdebug.h>

// Macro definition
#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

vtkStandardNewMacro(InteractorStyle);

// Interactor Methods
void InteractorStyle::OnChar()
{ 
  if(this->CurrentRenderer!=0)
  {
    char key = this->Interactor->GetKeyCode();
    if (key == 't' || key =='T')
    {
      int *eventPos = this->Interactor->GetEventPosition();
      this->FindPokedRenderer(eventPos[0], eventPos[1]);
      //qDebug() << "Mouse Position: [" << eventPos[0] << ", " << eventPos[1] << " ]";

      vtkSmartPointer<vtkWorldPointPicker> picker = vtkSmartPointer<vtkWorldPointPicker>::New();
      //qDebug() << "Picker class is: " << picker->GetClassName();
      picker->Pick(eventPos[0], eventPos[1], 0, this->CurrentRenderer);
      double picked[3];
      picker->GetPickPosition(picked);
      target->SetPosition(picked);
      
      this->Interactor->Render();
    }
  }
  else
  {
    vtkWarningMacro(<<"no current renderer on the interactor style.");
  }

  // Forward event
  vtkInteractorStyleTrackballCamera::OnChar();
}

void InteractorStyle::SetPlane(vtkSmartPointer<vtkActor> pl)
{
  plane = pl;
}

void InteractorStyle::SetTarget(vtkSmartPointer<vtkActor> tar)
{
  target = tar;
}